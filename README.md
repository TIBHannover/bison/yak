# Yak

Yak is the back end for the [B!SON open access journal recommender](https://projects.tib.eu/bison).
The project is written in Python using the Django framework and you can try out at [here](https://service.tib.eu/bison).

## Getting started
To run this project, first clone the repo (including `git lfs pull`).
Then run the following the commands to install the dependencies and run the project:
```
pip install -r requirements.txt
python manage.py migrate
python manage.py runserver
```

For development purposes, you can sub-class the settings in `bison/settings/dev.py` and modify them according to your needs (you might want to modify the variables for `DATABASES`, `CACHES`, `ELASTICSEARCH_DSL` and `ALLOWED_HOSTS`).

To start serving the ML models run:
```
ray start --head --port 8011
python serve_ml.py
```

For the front end, please have a look at the buffalo repository.

### Databases
The project uses a PostgreSQL and Elasticsearch database.
As the complete DOAJ data and OpenCitations' COCI dump is indexed, ~200 GB of disk space are needed for the download and the databases use around 300 GB of disk space.

If you want to download and equip the database, you have to set `DOWNLOAD_TEMP_DIR` in the settings and copy the `stopwords.txt` file to your location of choice and set the path in `ES_STOPWORDS_PATH`.
Then, execute the management commands:

```
python manage.py update_doaj
python manage.py update_es_index
python manage.py update_jct
python manage.py update_coci
python manage.py update_crossref_doaj_citations
python manage.py update_doaj_citation
python manage.py generate_journal_model
```

Depending on your system, this might take SEVERAL DAYS!
Make sure to disable Django debug mode to reduce memory consumption.


### API keys
For the feedback functionality to work, a Gitlab API key for issue creation is required. The setting is called `GITLAB_ISSUE_SECRET`.

For downloading the DOAJ dumps, an API key is required as well. The setting is called `DOAJ_KEY`.

## Architecture
The project follows the general Django project structure.
The `api` directory contains the publicly accessible API points and the recommender app contains most of the interesting functionality regarding data ingestion and journal recommendation.

The most important files are explained below:

```
.
├── api
│   └── api_views.py    → the API views
├── bison
│   └── settings        → all the application settings
├── extension
│   ├── admin.py        → configuration of models shown in the admin panel
│   ├── models.py       → models for the extension (mostly Extension itself)
│   └── serializers.py  → serialization of the extension models for the API
├── recommender
│   ├── currency.py     → currency conversion to €
│   ├── documents.py    → models for elasticsearch
│   ├── id_fetcher.py   → fetch title/abstract from Crossref, etc
│   ├── management      → shell commands to update the data
│   ├── ml              → code and weights for neural networks
│   ├── models          → models for the DOAJ, JCT, COCI data
│   ├── pre_filter.py   → retrieve language and subject prediction
│   ├── search.py       → the actual search algorithm
│   └── serializers.py  → serialize models to JSON
├── serve_ml.py         → runs ML models in separate ray processes
└── stopwords.txt       → stop words for elasticsearch filter
```


## Contributing
Contributions are more than welcome! Please use the usual workflow of forking the repo, creating a branch and making a merge request.

## License
All of the code in this repository is licensed under the AGPL license.
