import pickle

from datasets import ClassLabel
from fastapi import FastAPI
from lingua import LanguageDetectorBuilder
import logging
import ray
from ray import serve
from ray.serve.drivers import DAGDriver
from ray.serve.deployment_graph import InputNode
import torch
from transformers import AutoTokenizer

from recommender.ml.lm_bilstm_model import LmBilstmModel
from recommender.ml.scorer_model import ScorerModel

"""
Serve the models for classification etc. via ray serve.

Loading the models within the Django workers causes problems with the parallel
GPU access, load times and memory consumption.
To start, use:
$ ray start --head --port 8011
$ python serve_ml.py
$ curl "localhost:8012/Language/?text=hello%20world"
"""

logger = logging.getLogger("ray.serve")
app = FastAPI()


@serve.deployment
class Language:
    """Detect language of given text via lingua library."""
    def __init__(self):
        self.detector = LanguageDetectorBuilder.from_all_spoken_languages().build()

    async def forward(self, request):
        text = (await request.body()).decode()
        prediction = self.detector.detect_language_of(text)
        if prediction:
            prediction = prediction.iso_code_639_1.name
        return {"lang": prediction}


@serve.deployment(ray_actor_options={"num_gpus": 1/3})
class Subject:
    """
    Detect library of congress top level subject of the give text.

    This is a custom trained model based on the DOAJ articles and using
    xlm-roberta-large with a BiLSTM and a classification layer. The model is
    multilingual and currently has an accuracy of around 73.56%.
    """
    def __init__(self):
        self.classlabels = ClassLabel(names_file='recommender/ml/subjects.txt')
        self.model = LmBilstmModel.load_from_checkpoint(
            'recommender/ml/subject_model/model_weights',
            model_name='xlm-roberta-large',
            num_labels=len(self.classlabels.names),
            bilstm_size=256
        )
        self.model.eval()
        self.model.to(torch.device('cuda'))
        self.tokenizer = AutoTokenizer.from_pretrained(
            'xlm-roberta-large',
            use_fast=True
        )

    async def forward(self, request):
        text = (await request.body()).decode()
        tokenized = self.tokenizer(
            text,
            truncation=True,
            max_length=200,
            padding='max_length',
            return_tensors='pt'
        )
        tokenized.to(torch.device('cuda'))
        output = self.model(
            tokenized['input_ids'],
            tokenized['attention_mask']
        )[1][0]
        predicted = self.classlabels.int2str(int(torch.argmax(output)))
        probability = float(torch.max(output))
        if probability < 0.5:
            predicted = None
            probability = 0.0
        return {'subject': predicted, 'probability': probability}


@serve.deployment(ray_actor_options={"num_gpus": 1/3})
@serve.ingress(app)
class Scorer:
    """Combine and score the search results with trained neural network."""
    def __init__(self):
        self.model = ScorerModel.load_from_checkpoint(
            'recommender/ml/score_model/weights.ckpt',
            weights=torch.zeros(2)
        )
        self.model.eval()
        self.model.to(torch.device('cuda'))
        with open('recommender/ml/score_model/scaler.pkl', 'rb') as f:
            self.scaler = pickle.load(f)

    async def forward(self, request):
        decoded = await request.json()
        if len(decoded['scores']) == 0:
            return []
        scaled_scores = torch.Tensor(self.scaler.transform(decoded['scores']))
        scaled_scores = scaled_scores.to(torch.device('cuda'))
        results = self.model(scaled_scores)[1][:, 1]
        return results.detach().tolist()


@serve.deployment(ray_actor_options={"num_gpus": 1/3})
class Semantic:
    """
    Suggest journal for text (title plus abstract).

    This is a trained model with xlm-roberta-large + BiLSTM + dense layer like
    the subject model.
    Accuracy is around 26% for top@1 accuracy and 50% for top@5.
    """
    def __init__(self):
        self.classlabels = ClassLabel(
            names_file='recommender/ml/journal_classifier/journal_labels.txt'
        )
        self.model = LmBilstmModel.load_from_checkpoint(
            'recommender/ml/journal_classifier/weights.ckpt',
            model_name='xlm-roberta-large',
            num_labels=len(self.classlabels.names),
            bilstm_size=256,
            weights=torch.zeros(self.classlabels.num_classes)
        )
        self.model.eval()
        self.model.to(torch.device('cuda'))
        self.tokenizer = AutoTokenizer.from_pretrained(
            'xlm-roberta-large',
            use_fast=True
        )

    async def forward(self, request):
        text = (await request.body()).decode()
        if len(text) < 250:
            return {'journals': [], 'probabilities': []}

        tokenized = self.tokenizer(
            text,
            truncation=True,
            max_length=256,
            padding='max_length',
            return_tensors='pt'
        )
        tokenized.to(torch.device('cuda'))
        output = self.model(
            tokenized['input_ids'],
            tokenized['attention_mask']
        )[1][0]

        predicted = self.classlabels.int2str(output.argsort(descending=True)[:5])
        probabilities = sorted(output.tolist(), reverse=True)[:5]
        return {'journals': predicted, 'probabilities': probabilities}


if __name__ == '__main__':
    ray.init(address="localhost:8011", namespace="serve")
    with InputNode() as dag_input:
        subj_model = Subject.bind()
        sem_model = Semantic.bind()
        score_model = Scorer.bind()
        lang_model = Language.bind()
        d = DAGDriver.bind(
            {
                "/Subject": subj_model.forward.bind(dag_input),
                "/Semantic": sem_model.forward.bind(dag_input),
                "/Scorer": score_model.forward.bind(dag_input),
                "/Language": lang_model.forward.bind(dag_input)
            }
        )
    handle = serve.run(d, port=8012)
