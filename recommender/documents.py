from django.conf import settings
from django_elasticsearch_dsl import Document, fields
from django_elasticsearch_dsl.registries import registry
from elasticsearch_dsl import analyzer, analysis

from recommender.models import Article


stop_analyzer = analyzer(
    'multilang_stopwords',
    tokenizer="standard",
    filter=[
        'lowercase',
        analysis.token_filter(
            'stopper',
            'stop',
            stopwords_path=settings.ES_STOPWORDS_PATH
        )
    ]
)


@registry.register_document
class ArticleDocument(Document):
    class Index:
        name = 'articlesx'
        settings = {
            'number_of_shards': 4,
            'number_of_replicas': 0,
        }

    class Django:
        model = Article
        fields = [
            'idx'
        ]

    journal_issns = fields.ListField(fields.TextField())
    doi = fields.TextField()
    abstract = fields.TextField(analyzer=stop_analyzer)
    title = fields.TextField(analyzer=stop_analyzer)
