import logging
from typing import List
from django.db.models import Q
from rest_framework import serializers
from drf_spectacular.utils import extend_schema_field
from drf_spectacular.types import OpenApiTypes

from recommender.currency import (convert_currency, ExchangeRateUnknownException,
                                  ExchangeRatesUnavailableException)
from recommender.models import (Journal, ApcMax, PlansCompliance, Subject,
                                License)


logger = logging.getLogger(__name__)


class LicenseSerializer(serializers.ModelSerializer):
    class Meta:
        model = License
        exclude = ['id']


class SubjectSerializer(serializers.ModelSerializer):
    class Meta:
        model = Subject
        fields = ['term', 'code']


class ApcMaxSerializer(serializers.ModelSerializer):
    class Meta:
        model = ApcMax
        exclude = ['id']

    euro = serializers.SerializerMethodField()

    @extend_schema_field(OpenApiTypes.NUMBER)
    def get_euro(self, obj):
        try:
            return convert_currency(obj.price, obj.currency)
        except (ExchangeRatesUnavailableException, ExchangeRateUnknownException):
            return None


class RecommendedArticleSerializer(serializers.Serializer):
    # Dummy class which is only for swagger API doc.
    title = serializers.CharField(required=True)
    doi = serializers.CharField(required=False)


class ScoreSerializer(serializers.Serializer):
    # Dummy class which is only for swagger API doc.
    value = serializers.FloatField(required=True)
    title = RecommendedArticleSerializer(required=True, many=True)
    abstract = RecommendedArticleSerializer(required=True, many=True)
    dois = RecommendedArticleSerializer(required=True, many=True)
    semantic_score = serializers.FloatField(required=True)


class JournalSerializer(serializers.ModelSerializer):
    score = serializers.SerializerMethodField(required=False)
    plan_s_compliance = serializers.SerializerMethodField()
    apc_max = serializers.SerializerMethodField()
    id = serializers.SerializerMethodField()
    licenses = LicenseSerializer(many=True)
    subjects = SubjectSerializer(many=True)

    class Meta:
        model = Journal
        fields = '__all__'

    def get_id(self, obj) -> str:
        return obj.idx

    def get_apc_max(self, obj) -> ApcMaxSerializer:
        if not obj.apc_max.exists():
            return {}
        elif obj.apc_max.filter(currency='EUR').exists():
            return ApcMaxSerializer(
                obj.apc_max.filter(currency='EUR').first()
            ).data
        else:
            return ApcMaxSerializer(obj.apc_max.first()).data

    def get_score(self, obj) -> ScoreSerializer:
        if hasattr(obj, 'score'):
            return obj.score
        else:
            return {}

    def get_plan_s_compliance(self, obj) -> bool:
        try:
            compliance = PlansCompliance.objects.filter(Q(issn=obj.pissn) |
                                                        Q(issn=obj.eissn))\
                                                .first()
            return compliance.compliant
        except Exception:
            logger.info(
                f'Failed to fetch Plan-S info for {obj.pissn}/{obj.eissn}'
            )
            return False


class SearchSerializer(serializers.Serializer):
    # Dummy class which is only for swagger API doc.
    title = serializers.CharField(required=False)
    abstract = serializers.CharField(required=False)
    references = serializers.CharField(required=False)
