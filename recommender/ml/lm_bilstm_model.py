from transformers import AutoConfig, AutoModel
import pytorch_lightning as pl
import torch
import torchmetrics


class LmBilstmModel(pl.LightningModule):
    """
    Model for subject/journal classification.

    A model built with pytorch-lightning using a Hugginface language model 
    with frozen weights and a BiLSTM and linear layer.
    """

    def __init__(self, model_name, num_labels, bilstm_size, weights=None):
        super().__init__()

        hg_config = AutoConfig.from_pretrained(model_name,
                                               output_attentions=True,
                                               output_hidden_states=True)
        self.model = AutoModel.from_pretrained(
            model_name,
            config=hg_config
        )
        for param in self.model.parameters():
            param.requires_grad = False

        self.bilstm = torch.nn.LSTM(
            input_size=self.model.config.hidden_size,
            hidden_size=bilstm_size,
            num_layers=1,
            bidirectional=True,
            batch_first=True,
            bias=True
        )
        self.dropout = torch.nn.Dropout(0.2)
        self.classifier = torch.nn.Linear(bilstm_size,
                                          num_labels)
        self.softmax = torch.nn.Softmax(dim=1)
        self.loss_fn = torch.nn.CrossEntropyLoss()
        if weights is not None:
            self.loss_fn = torch.nn.CrossEntropyLoss(weight=weights)

    def forward(self, input_ids, attention_mask=None, labels=None):
        output = self.model(input_ids=input_ids,
                            attention_mask=attention_mask)
        output, (hn, cn) = self.bilstm(output.last_hidden_state)
        output = self.dropout(hn[-1])
        output = self.classifier(output)

        loss = None
        if labels is not None:
            loss = self.loss_fn(output, labels)
        else:
            output = self.softmax(output)

        return loss, output

    def training_step(self, batch, batch_idx):
        input_ids = batch["input_ids"]
        attention_mask = batch["attention_mask"]
        labels = batch["labels"]
        loss, outputs = self(input_ids, attention_mask, labels)

        self.log("train_loss", loss, prog_bar=True, logger=True)
        accuracy = torchmetrics.functional.accuracy(outputs, labels)
        self.log("train_acc", accuracy, prog_bar=True, logger=True)

        return {"loss": loss, "predictions": outputs, "labels": labels}

    def validation_step(self, batch, batch_idx):
        input_ids = batch["input_ids"]
        attention_mask = batch["attention_mask"]
        labels = batch["labels"]
        loss, outputs = self(input_ids, attention_mask, labels)
        self.log("val_loss", loss, prog_bar=True, logger=True)

        accuracy = torchmetrics.functional.accuracy(outputs, labels)
        self.log("val_acc", accuracy, prog_bar=True, logger=True)
        return loss

    def test_step(self, batch, batch_idx):
        input_ids = batch["input_ids"]
        attention_mask = batch["attention_mask"]
        labels = batch["labels"]
        loss, outputs = self(input_ids, attention_mask, labels)

        accuracy = torchmetrics.functional.accuracy(outputs, labels)
        self.log(f"test_acc@1", accuracy, prog_bar=True, logger=True)
        accuracy5 = torchmetrics.functional.accuracy(outputs, labels,
                                                     top_k=5)
        self.log(f"test_acc@5", accuracy5, prog_bar=True, logger=True)
        accuracy10 = torchmetrics.functional.accuracy(outputs, labels,
                                                      top_k=10)
        self.log("test_acc@10", accuracy10, prog_bar=True, logger=True)
        accuracy15 = torchmetrics.functional.accuracy(outputs, labels,
                                                      top_k=15)
        self.log("test_acc@15", accuracy15, prog_bar=True, logger=True)

        return accuracy, accuracy5, accuracy10, accuracy15


    def predict_step(self, batch, batch_idx, dataloader_idx=0):
        return self(batch)

    def configure_optimizers(self):
        return torch.optim.Adam(self.parameters(), lr=1.5e-3)
