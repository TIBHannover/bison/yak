import pytorch_lightning as pl
import torch


class ScorerModel(pl.LightningModule):
    """
    Model which predicts whether journal is correct.
    
    It receives four scores (for title, abstract, references, and semantic
    model) and predicts whether the journal is correct based on the dataset of
    simulated searches. The output is the probability shown to the user.
    """
    def __init__(self, weights=None):
        super().__init__()

        self.dropout = torch.nn.Dropout(0.1)
        self.dense1 = torch.nn.Linear(4, 4)
        self.dense2 = torch.nn.Linear(4, 2)
        self.softmax = torch.nn.Softmax(dim=1)
        self.loss_fn = torch.nn.CrossEntropyLoss(weights)

    def forward(self, inputs, labels=None):
        output = self.dropout(inputs)
        output = self.dense1(inputs)
        output = self.dropout(inputs)
        output = self.dense2(inputs)

        loss = None
        if labels is not None:
            loss = self.loss_fn(output, labels)
        else:
            output = self.softmax(output)

        return loss, output

    def training_step(self, batch):
        loss, outputs = self(*batch)
        accuracy = accuracy(outputs, batch[1])
        self.log("train_loss", loss, prog_bar=True, logger=True)
        self.log("train_acc", accuracy, prog_bar=True, logger=True)
        return {"loss": loss, "predictions": outputs, "labels": batch[1]}

    def validation_step(self, batch):
        loss, outputs = self(*batch)
        accuracy = accuracy(outputs, batch[1])
        self.log("val_loss", loss, prog_bar=True, logger=True)
        self.log("val_acc", accuracy, prog_bar=True, logger=True)
        return loss

    def configure_optimizers(self):
        return torch.optim.Adam(self.parameters(), lr=1.5e-3)
