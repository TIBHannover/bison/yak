import json
import logging

import requests
from django.core.cache import cache


logger = logging.getLogger(__name__)


def convert_currency(value, currency):
    """Convert value in currency to euro."""
    if currency == 'EUR':
        return value

    # update exchange rates if they are not in the cache and keep for 24 hours
    if 'exchange_rates' not in cache:
        try:
            response = requests.get('https://api.exchangerate-api.com/v4/latest/EUR')
            rates = json.loads(response.text)['rates']
            cache.set('exchange_rates', rates, 60*60*24)
        except Exception as e:
            # try again in one hour
            cache.set('exchange_rates', None, 60)
            logger.warning('Updating exchange rates failed')
            logger.exception(e)
            return ExchangeRatesUnavailableException()
    else:
        rates = cache.get('exchange_rates')
        if rates is None:
            logger.warning('Exchange rates unavailable')
            return ExchangeRatesUnavailableException()

    if currency not in rates:
        logger.warning(f'Unknown exchange rate for {currency}')
        raise ExchangeRateUnknownException(f'Unknown rate for {currency}')
    return round(value / rates[currency], 2)


class ExchangeRateUnknownException(Exception):
    pass


class ExchangeRatesUnavailableException(Exception):
    pass
