import json
import logging
import os
from pathlib import Path
import random
import shutil

from cachalot.api import cachalot_disabled
from datasets import load_dataset, ClassLabel
from django.core.management.base import BaseCommand
import pytorch_lightning as pl
from sklearn.utils.class_weight import compute_class_weight
import torch
from transformers import AutoTokenizer
from tqdm import tqdm

from recommender.ml.lm_bilstm_model import LmBilstmModel
from recommender.models import Article, Journal


logger = logging.getLogger(__name__)


class Command(BaseCommand):
    help = 'Generate dataset and train journal classifier model.'
    base_path = Path('journal_classifier')

    def add_arguments(self, parser):
        parser.add_argument('--generate_only', action='store_true')
        parser.add_argument('--train_only', action='store_true')

    def handle(self, *args, **options):
        if not os.path.exists(self.base_path):
            logger.info('Creating directory to for NN training')
            os.mkdir(self.base_path)

        try:
            if not options['train_only']:
                logger.info('Generate journal classifier dataset')
                self.generate_dataset()

            if not options['generate_only']:
                logger.info('Train journal classifier model')
                self.train_model()
        except Exception:
            logger.exception('Exception while updating journal classifier')

    def generate_dataset(self):
        """Generate dataset of X articles per journal to train classifier."""
        min_articles = 30
        test_articles = 3
        train_articles = 60

        # filter for articles containing title and abstract
        journals = {}
        with cachalot_disabled():
            for article in tqdm(Article.objects.all().iterator()):
                journal = Journal.get_journal(article.journal_issns)
                if journal is None:
                    continue

                journals.setdefault(journal.idx, [])
                if len(journals[journal.idx]) > test_articles + train_articles:
                    continue
                if not article.title or len(article.title) < 30:
                    continue
                if not article.abstract or len(article.abstract) < 150:
                    continue

                journals[journal.idx].append(article.idx)

        # write articles to file
        filename = str(self.base_path / 'journal_classification')
        with open(filename + '.test.jsonl', 'w') as f_test,\
             open(filename + '.jsonl', 'w') as f:
            for journal, articles in tqdm(journals.items()):
                if len(articles) < min_articles:
                    # skip journals with too few articles
                    continue

                sample = random.sample(
                    articles,
                    min(len(articles), test_articles + train_articles)
                )
                for article_idx in sample[:test_articles]:
                    article = Article.objects.get(idx=article_idx)
                    journal = Journal.get_journal(article.journal_issns)
                    f_test.write(json.dumps({
                        'idx': article.idx,
                        'doi': article.doi,
                        'title': article.title,
                        'abstract': article.abstract,
                        'journal': journal.idx}
                    ) + '\n')

                for article_idx in sample[test_articles:]:
                    article = Article.objects.get(idx=article_idx)
                    journal = Journal.get_journal(article.journal_issns)
                    f.write(json.dumps({
                        'idx': article.idx,
                        'doi': article.doi,
                        'title': article.title,
                        'abstract': article.abstract,
                        'journal': journal.idx}
                    ) + '\n')

        with open(filename + '.labels.txt', 'w') as f:
            filtered_journals = filter(lambda j: len(journals[j]) >= min_articles,
                                       journals.keys())
            for journal in filtered_journals:
                f.write(journal + '\n')

    def train_model(self):
        """
        Train journal classifier with article title and abstract.


        The model is pre-trained xlm-roberta-large with BiLSTM and dense layer.
        """
        labels_file = self.base_path / 'journal_classification.labels.txt'
        train_file = str(self.base_path / 'journal_classification.jsonl')
        test_file = str(self.base_path / 'journal_classification.test.jsonl')
        cache_dir = self.base_path / 'cache'
        max_tokens = 256
        model_name = 'xlm-roberta-large'
        val_split = 0.001
        batch_size = 50
        epochs = 5

        # create cache directory
        if not os.path.exists(cache_dir):
            os.mkdir(cache_dir)

        classlabels = ClassLabel(names_file=labels_file)

        tokenizer = AutoTokenizer.from_pretrained(model_name,
                                                  model_max_length=max_tokens)

        dataset = load_dataset('json',
                               data_files=train_file)
        dataset = dataset.shuffle()

        # load first and split now, so we can first shuffle
        dataset = dataset['train'].train_test_split(val_split)
        train_dataset = dataset['train']
        val_dataset = dataset['test']
        test_dataset = load_dataset('json',
                                    data_files=test_file)

        def tokenize(batch):
            text = list(map(lambda x: ' '.join(x),
                            zip(batch['title'], batch['abstract'])))
            tokens = tokenizer(text,
                               max_length=max_tokens,
                               return_token_type_ids=False,
                               padding="max_length",
                               truncation=True)
            tokens['labels'] = classlabels.str2int(batch['journal'])
            return tokens

        val_dataset = val_dataset.map(
            tokenize,
            batched=True,
            remove_columns=['journal', 'title', 'abstract'],
            cache_file_name=str(cache_dir / 'val_articles')
        )
        val_dataset.set_format(type='torch',
                               columns=['input_ids',
                                        'attention_mask',
                                        'labels'])

        train_dataset = train_dataset.map(
            tokenize,
            batched=True,
            remove_columns=['journal', 'title', 'abstract'],
            cache_file_name=str(cache_dir / 'train_articles')
        )
        train_dataset.set_format(type='torch',
                                 columns=['input_ids',
                                          'attention_mask',
                                          'labels'])

        test_dataset = test_dataset.map(
            tokenize,
            batched=True,
            remove_columns=['journal', 'title', 'abstract'],
            cache_file_names={k: str(cache_dir / f'test_articles_{k}')
                              for k in test_dataset}
        )
        test_dataset.set_format(type='torch',
                                columns=['input_ids',
                                         'attention_mask',
                                         'labels'])

        weights = compute_class_weight(
            'balanced',
            classes=list(range(classlabels.num_classes)),
            y=train_dataset['labels'].tolist()
        )
        weights = torch.FloatTensor(weights)

        train_dataloader = torch.utils.data.DataLoader(train_dataset,
                                                       num_workers=10,
                                                       shuffle=True,
                                                       batch_size=batch_size)
        val_dataloader = torch.utils.data.DataLoader(val_dataset,
                                                     num_workers=10,
                                                     batch_size=batch_size)
        test_loader = torch.utils.data.DataLoader(test_dataset['train'],
                                                  batch_size=batch_size)

        early_stopping_callback = pl.callbacks.EarlyStopping(monitor='val_loss',
                                                             patience=2)

        checkpoint_callback = pl.callbacks.ModelCheckpoint(
            dirpath=self.base_path,
            filename="best-checkpoint",
            save_top_k=1,
            verbose=True,
            monitor="test_acc@10",
            mode="max"
        )

        trainer = pl.Trainer(
            checkpoint_callback=checkpoint_callback,
            default_root_dir=self.base_path,
            log_every_n_steps=1,
            callbacks=[early_stopping_callback],
            max_epochs=epochs,
            gpus=1,
            progress_bar_refresh_rate=1
        )

        model = LmBilstmModel(
            model_name='xlm-roberta-large',
            num_labels=classlabels.num_classes,
            bilstm_size=256,
            weights=weights
        )
        trainer.fit(model, train_dataloader, val_dataloader)
        test_result = trainer.test(dataloaders=test_loader)[0]

        if test_result['test_acc@1'] < 0.2:
            logger.info(f'Low accuracy ({test_result["test_acc@1"]}), discarding')
            shutil.rmtree(self.base_path)
            return

        # save weights and delete the rest
        if not os.path.exists('recommender/ml/journal_classifier'):
            os.mkdir('recommender/ml/journal_classifier')
        shutil.move(
            list(self.base_path.rglob('*ckpt'))[-1],
            'recommender/ml/journal_classifier/weights.ckpt'
        )
        shutil.move(self.base_path / 'journal_classification.labels.txt',
                    'recommender/ml/journal_classifier/journal_labels.txt')
        shutil.rmtree(self.base_path)
