from datetime import timedelta
import logging
import time

from cachalot.api import cachalot_disabled
from django.core.management.base import BaseCommand
from django.db import transaction
import requests
from tqdm import tqdm

from recommender.models import Journal, DatasetState, CrossrefArticle


logger = logging.getLogger(__name__)


class Command(BaseCommand):
    help = 'Create database with subset of citations with DOAJ journal articles in crossref.'
    dataset = 'Crossref'

    def handle(self, *args, **options):
        DatasetState(dataset=self.dataset,
                     state=DatasetState.State.RUNNING).save()
        try:
            self.update()
            DatasetState(dataset=self.dataset,
                         state=DatasetState.State.UPDATED).save()
        except Exception as e:
            logger.exception('EXCEPTIONS WHILE UPDATING CROSSREF ARTICLES')
            DatasetState(dataset=self.dataset,
                         state=DatasetState.State.FAILED).save()

    def update(self):
        logger.info('Start updating crossref articles')

        # if there is a previous successfull update we only fetch from that
        # point on
        state = DatasetState.objects.filter(dataset=self.dataset,
                                            state=DatasetState.State.UPDATED)\
                                    .order_by('-date')\
                                    .first()
        if state:
            state_date = (state.date + timedelta(days=1)).strftime('%Y-%m-%d')
        else:
            state_date = None

        with transaction.atomic(), cachalot_disabled():
            errors = 0
            for journal in tqdm(Journal.objects.all(), smoothing=0.05):
                for issn in [journal.pissn, journal.eissn]:
                    if issn is None:
                        continue

                    articles = []

                    time.sleep(0.5)  # small delay between requests

                    query_filter = 'issn:' + issn
                    if state_date:
                        query_filter += ',from-created-date:' + state_date
                    # we start with a '*' cursor and update it if the result
                    # contains so many items that we need to make several
                    # requests with an updated cursor
                    cursor = '*'
                    num_results = 1  # set placeholer before knowing the value

                    try:
                        while len(articles) < num_results:
                            req_time = time.time()
                            resp = requests.get(
                                'https://api.crossref.org/works',
                                params={
                                    'filter': query_filter,
                                    'rows': 1000,
                                    'select': 'ISSN,DOI',
                                    'cursor': cursor
                                },
                                headers={'User-Agent': 'BISON-issn-fetcher; bison@tib.eu'},
                                timeout=100
                            )
                            jresp = resp.json()
                            for article in jresp['message']['items']:
                                if issn not in article['ISSN']:
                                    continue
                                articles.append(
                                    CrossrefArticle(issn=issn,
                                                    doi=article['DOI'])
                                )
                            num_results = jresp['message']['total-results']
                            cursor = jresp['message']['next-cursor']
                            rate_limit = int(resp.headers['x-rate-limit-interval'][:-1]) / int(resp.headers['x-rate-limit-limit'])
                            req_time = time.time() - req_time
                            if req_time < rate_limit:
                                time.sleep(rate_limit - req_time)
                    except Exception as e:
                        logger.warning(
                            f'Exception while updating {issn}: {str(e)}'
                        )
                        time.sleep(100)

                        errors += 1
                        if errors > 100:
                            raise Exception('Too many errors (>100)')

                    CrossrefArticle.objects.bulk_create(articles)

        logger.info('Updating crossref articles done')
