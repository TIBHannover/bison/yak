import json
import logging
import os

from cachalot.api import cachalot_disabled
from datasets import load_dataset, ClassLabel
from django.core.management.base import BaseCommand
import torch
import pytorch_lightning as pl
from tqdm import tqdm
from transformers import AutoTokenizer

from recommender.models import Article, Journal
from recommender.ml.lm_bilstm_model import LmBilstmModel


"""
Generate a model to predict the top-level subject (according to LCC)
based on DOAJ data. The model is built with huggingface by adding a dense layer
classification on top of a 'xlm-roberta-large' multilingual model.
"""


logger = logging.getLogger(__name__)


class Command(BaseCommand):
    help = 'Generate dataset and train subject prediction model.'

    def add_arguments(self, parser):
        parser.add_argument('--generate_only', action='store_true')
        parser.add_argument('--train_only', action='store_true')

    def handle(self, *args, **options):
        if not options['train_only']:
            logger.info('Generate subject dataset')
            self.generate_dataset()

        if not options['generate_only']:
            logger.info('Train subject model')
            self.train_model()

    def generate_dataset(self):
        subjects = {
            'A': 'General Works',
            'B': 'Philosophy. Psychology. Religion',
            'C': 'Auxiliary sciences of history',
            'D': 'History (General) and history of Europe',
            'E': 'History America',
            'F': 'History America',
            'G': 'Geography. Anthropology. Recreation',
            'H': 'Social Sciences',
            'J': 'Political science',
            'K': 'Law',
            'L': 'Education',
            'M': 'Music and books on Music',
            'N': 'Fine Arts',
            'P': 'Language and Literature',
            'Q': 'Science',
            'R': 'Medicine',
            'S': 'Agriculture',
            'T': 'Technology',
            'U': 'Military Science',
            'V': 'Naval Science',
            'Z': 'Bibliography. Library science. Information resources',
        }
        with open('recommender/ml/subjects.txt', 'w') as f:
            for val in set(subjects.values()):
                f.write(val + '\n')

        # generate dataset with 10_000 articles per subject, 10% for testing
        articles_per_subject = {}
        test_articles_per_subject = {}
        with open('articles_subject.jsonl', 'w') as f_train,\
             cachalot_disabled(),\
             open('articles_subject.test.jsonl', 'w') as f_test:
            for article in tqdm(Article.objects.all().iterator()):
                journal = Journal.get_journal(article.journal_issns)
                if journal is None:
                    continue

                try:
                    code = journal.subjects.first().code[0]
                except:
                    logger.exception(f'Error fetching subject code for {journal}')
                    continue

                text = (article.title or '') + ' ' + (article.abstract or '')
                text = text.replace('\n', '\\n')

                # ignore short texts
                if len(text) < 130:
                    continue

                subject = subjects[code]

                jsonl_line = json.dumps(
                    {'text': text, 'subject': subject}
                ) + '\n'

                articles_per_subject.setdefault(subject, 0)
                test_articles_per_subject.setdefault(subject, 0)

                if test_articles_per_subject[subject] < 1_000:
                    f_test.write(jsonl_line)
                    test_articles_per_subject[subject] += 1
                elif articles_per_subject[subject] < 9_000:
                    articles_per_subject[subject] += 1
                    f_train.write(jsonl_line)

    def train_model(self):
        train_file = 'articles_subject.test.jsonl'
        test_file = 'articles_subject.jsonl'
        classlabels_file = 'recommender/ml/subjects.txt'
        max_length = 200
        model_name = 'xlm-roberta-large'

        # prepare dataset
        dataset = load_dataset('json', data_files=train_file)
        dataset = dataset.shuffle()
        dataset = dataset['train'].train_test_split(0.1)

        classlabels = ClassLabel(names_file=classlabels_file)

        tokenizer = AutoTokenizer.from_pretrained(model_name)

        def tokenize_function(batch):
            tokens = tokenizer(batch['text'],
                               truncation=True,
                               max_length=max_length,
                               padding='max_length')
            tokens['labels'] = classlabels.str2int(batch['subject'])
            return tokens

        tokenized_datasets = dataset.map(
            tokenize_function,
            remove_columns=['text', 'subject'],
            batched=True
        )

        tokenized_datasets.set_format(type='torch',
                                      columns=['input_ids',
                                              'attention_mask',
                                              'labels'])

        # prepare test dataset
        test_dataset = load_dataset('json', data_files=test_file)
        test_tokenized_datasets = test_dataset.map(
            tokenize_function,
            remove_columns=['text', 'subject'],
            batched=True
        )
        test_tokenized_datasets.set_format(type='torch',
                                           columns=['input_ids',
                                                       'attention_mask',
                                                       'labels'])

        train_dataloader = torch.utils.data.DataLoader(tokenized_datasets['train'],
                                                       shuffle=True,
                                                       batch_size=50)
        val_dataloader = torch.utils.data.DataLoader(tokenized_datasets['test'],
                                                     batch_size=50)
        test_dataloader = torch.utils.data.DataLoader(test_tokenized_datasets['train'],
                                                      batch_size=50)

        # train model
        early_stopping_callback = pl.callbacks.EarlyStopping(monitor='val_loss', 
                                                             patience=4)

        checkpoint_callback = pl.callbacks.ModelCheckpoint(
            dirpath=os.path.join('lastlayer_tune', "checkpoints"),
            filename="best-checkpoint",
            save_top_k=1,
            verbose=True,
            monitor="test_acc@1",
            mode="max"
        )

        trainer = pl.Trainer(
            checkpoint_callback=checkpoint_callback,
            default_root_dir=os.path.join('recommender', 'ml', 'subject_model'),
            callbacks=[early_stopping_callback],
            max_epochs=10,
            gpus=1,
            progress_bar_refresh_rate=1
        )

        model = LmBilstmModel(model_name, classlabels.num_classes, 256)
        trainer.fit(model, train_dataloader, val_dataloader)
        trainer.test(model, test_dataloader)
