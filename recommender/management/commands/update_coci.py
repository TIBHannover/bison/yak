import csv
from datetime import datetime, timedelta
import itertools
import json
import logging
import os
from pathlib import Path
import requests
import shutil
from zipfile import ZipFile

from django.conf import settings
from django.core.management.base import BaseCommand
from django.db import connection, transaction
from django.db.models import Q
from django.utils import timezone
from tqdm.contrib.concurrent import process_map
from tqdm import tqdm

from recommender.models.general import DatasetState


logger = logging.getLogger(__name__)


class Command(BaseCommand):
    help = 'Insert/Update the COCI (now OpenCitations Meta) data.'
    dataset = 'OpenCitations Meta'

    def add_arguments(self, parser):
        parser.add_argument('--new_index', action='store_true')

    def handle(self, *args, **options):
        update_query = Q(dataset=self.dataset) & Q(state=DatasetState.State.UPDATED)
        if DatasetState.objects.filter(update_query).exists():
            date = DatasetState.objects.filter(update_query)\
                                       .order_by('-date')\
                                       .first()\
                                       .date
        else:
            date = timezone.now() - timedelta(days=10000)

        logger.info('Start updating COCI (OpenCitations Meta) data')

        update_state = DatasetState(dataset=self.dataset,
                                    state=DatasetState.State.RUNNING)
        update_state.save()

        path = Path(settings.DOWNLOAD_TEMP_DIR) / 'coci'
        path.mkdir(parents=True, exist_ok=True)

        try:
            files = download_files(path, date)
            if len(files) == 0:
                logger.debug('No new files. Nothing todo.')
                update_state.delete()
                return

            if options['new_index']:
                delete_index()

            to_path = path / 'csv_dois'
            to_path.mkdir(parents=True, exist_ok=True)

            mapping_path = download_mapping(path)
            convert_omid_to_dois(mapping_path, files, to_path)

            logger.debug('COPY CSV files into DB')
            with transaction.atomic(), connection.cursor() as cursor:
                for file_path in tqdm(to_path.glob('*')):
                    cursor.execute(f"""
                        COPY recommender_citation (frm, tow)
                        FROM '{file_path}'
                        DELIMITER ','
                        CSV
                    """)

            create_index()
            logger.info('Updating COCI data done')
            DatasetState(dataset=self.dataset,
                         state=DatasetState.State.UPDATED).save()
        except Exception:
            logger.exception('EXCEPTION WHILE UPDATING COCI')
            DatasetState(dataset=self.dataset,
                         state=DatasetState.State.FAILED).save()
        shutil.rmtree(path)


def delete_index():
    logger.debug('Deleting indices')
    with connection.cursor() as c:
        c.execute("""SELECT indexname
                     FROM pg_indexes
                     WHERE tablename = 'recommender_citation';""")
        indexes = c.fetchall()
    for index in indexes:
        index = index[0]
        if 'pkey' in index:
            continue
        with connection.cursor() as c:
            c.execute("DROP INDEX " + index)
    logger.debug('Index deletion done')


def create_index():
    with connection.cursor() as c:
        c.execute("""SELECT indexname
                     FROM pg_indexes
                     WHERE tablename = 'recommender_citation';""")
        indexes = [index[0] for index in c.fetchall()]
        if 'recommender_citation_frm' in indexes:
            logger.debug('Indices already exist')
            return
    with connection.cursor() as c:
        logger.debug('Creating new indices')
        c.execute(
            "CREATE INDEX recommender_citation_frm ON recommender_citation(frm)"
        )
        c.execute(
            "CREATE INDEX recommender_citation_tow ON recommender_citation(tow)"
        )
    logger.debug('Index creation done')


def download_files(path, prev_ingest_date):
    """Download new COCI files and return file paths.

    path: File path to download files to
    prev_ingest_date: datetime.date when last data ingest happened
    """
    # check API for new data and download
    api = requests.get('https://api.figshare.com/v2/articles/24356626')
    api = json.loads(api.content)

    for file in api['files']:
        file_time = datetime.strptime(file['name'][:10].replace('-', '_'), '%Y_%m_%d')
        file_time = file_time.replace(tzinfo=timezone.utc)
        if file_time <= prev_ingest_date:
            continue
        logger.debug(f'Downloading {file["name"]}')
        with requests.get(file['download_url'], stream=True) as r:
            with open(path / file['name'], 'wb') as f:
                for chunk in r.iter_content(chunk_size=16*1024):
                    f.write(chunk)

    # unzip inner zips
    logger.debug('Unpacking zips')
    files = path.glob('*zip')
    for file in files:
        ZipFile(file).extractall(path=path)
        file.unlink()

    files = list(path.rglob('*.csv'))
    return files


def download_mapping(path):
    """Download OMID -> DOI mapping from figshare."""
    api = requests.get('https://api.figshare.com/v2/articles/24427156').json()

    mapping_file = api['files'][0]
    logger.debug(f'Downloading {mapping_file["name"]}')
    with requests.get(mapping_file['download_url'], stream=True) as r:
        zip_path = path / 'mapping.zip'
        with open(zip_path, 'wb') as f:
            for chunk in r.iter_content(chunk_size=16*1024):
                f.write(chunk)

    ZipFile(zip_path).extractall(path=path)
    return path / 'omid.csv'


def convert_omid_to_dois(mapping_path, files, to_path):
    """Convert OpenCitations' OMID identifier to DOI."""
    logger.info('Loading the mapping will take >15GB of memory')
    mapping = {}
    with open(mapping_path) as f:
        for line in csv.reader(f):
            omid = line[0]
            dois = [
                idx.removeprefix('doi:')
                for idx in line[1].split(' ')
                if idx.startswith('doi:') and not '\\' in idx
            ]
            mapping[omid] = ' '.join(dois)

    logger.info('Converting citations from OMID <-> OMID to DOI <-> DOI')
    missing_mapping_count = 0
    for file_path in tqdm(files):
        with open(file_path) as f_in, open(to_path / file_path.name, 'w') as f_out:
            writer = csv.writer(f_out)
            reader = csv.reader(f_in)
            next(reader)
            for line in reader:
                try:
                    frm_dois = mapping[line[1].removeprefix('omid:')].split(' ')
                    tow_dois = mapping[line[2].removeprefix('omid:')].split(' ')
                    for frm, tow in itertools.product(frm_dois, tow_dois):
                        if frm and tow:
                            writer.writerow((frm, tow))
                except KeyError:
                    missing_mapping_count += 1
    logger.warning(
        f'{missing_mapping_count} citations missing because OMIDs were not found in mapping'
    )

