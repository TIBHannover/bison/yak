import json
import logging
import os
from pathlib import Path
import re
import shutil
import tarfile
import unicodedata

from bs4 import BeautifulSoup
from django.conf import settings
from django.core.management.base import BaseCommand
from django.db import transaction, connection
from django.conf import settings
import requests

from recommender.models import (Article, Journal, Keyword, Subject, Language,
                                IsReplacedBy, ApcMax, License,
                                EditorialReviewProcess, DatasetState)
from recommender.search import extract_dois


logger = logging.getLogger(__name__)


def uniq_dicts(dict_list):
    """Remove duplicated from a list of dicts."""
    return [dict(s) for s in set(frozenset(d.items()) for d in dict_list)]


def flatten(t):
    """Flatten a list of lists"""
    return [item for sublist in t for item in sublist]


def int_or_none(x):
    """Convert value to int or return None."""
    try:
        return int(x)
    except (ValueError, TypeError):
        return None


def pre_process_text(text):
    """Pre-process text by stripping urls, HTML etc."""
    if type(text) != str:
        return text

    # replace_null_byte
    text = text.replace("\x00", "\uFFFD")

    # normalize unicode
    text =  unicodedata.normalize('NFKD', text)

    # remove URLs
    text = re.sub(r'https?://\S+|www\.\S+', '', text)

    # strip HTML tags
    soup = BeautifulSoup(text, "html.parser")
    text = soup.get_text(separator=" ")

    # remove white space
    text = " ".join(text.split())

    # strip non-UTF-8
    text = text.encode('utf-8','ignore').decode("utf-8")

    return text


class Command(BaseCommand):
    help = 'Insert/Update the DOAJ data.'
    dataset = 'DOAJ'

    def add_arguments(self, parser):
        parser.add_argument('--journals-only', action='store_true')
        parser.add_argument('--articles-only', action='store_true')

    def handle(self, *args, **options):
        DatasetState(dataset=self.dataset,
                     state=DatasetState.State.RUNNING).save()

        try:
            if options['journals_only']:
                self.insert_journals()
            elif options['articles_only']:
                self.insert_articles()
            else:
                self.insert_journals()
                self.insert_articles()
            DatasetState(dataset=self.dataset,
                         state=DatasetState.State.UPDATED).save()
        except Exception:
            logger.exception('EXCEPTION WHEN UPDATING DOAJ DATA')
            DatasetState(dataset=self.dataset,
                         state=DatasetState.State.FAILED).save()

    def insert_articles(self):
        """Iterate through article batches and insert/update them."""
        url = 'https://doaj.org/public-data-dump/article?api_key=' + settings.DOAJ_KEY
        files = self.download_unpack(url)

        logger.info('Updating DOAJ articles: START')

        with transaction.atomic():
            # delete with raw SQL otherwise django apparently creates objects
            # in memory which results in huge RAM consumption
            logger.debug('Truncating article table')
            table = Article.objects.model._meta.db_table
            with connection.cursor() as cursor:
                cursor.execute('TRUNCATE ' + table + ' CASCADE')

            data = []
            for filename in files:
                logger.debug(f'Inserting file: {filename}')
                with open(filename) as f:
                    for line in f:
                        # parse one json object per line
                        line = line.strip()\
                                .removeprefix('[')\
                                .removesuffix(']')\
                                .removesuffix(',')
                        parsed = json.loads(line)
                        data.append(self.parse_article(parsed))
                if len(data) > 2_000_000:
                    # save articles (already in between to reduce memory)
                    articles = map(lambda e: Article(**e), data)
                    Article.objects.bulk_create(articles, batch_size=50000)
                    data = []
            articles = map(lambda e: Article(**e), data)
            Article.objects.bulk_create(articles, batch_size=50000)

        shutil.rmtree(files[0].parent)
        logger.info('Updating DOAJ articles: DONE')

    def parse_article(self, parsed):
        """Insert/Update one json-formatted article in db."""
        # only save DOI identifier
        doi = None
        for ident in parsed['bibjson'].get('identifier', []):
            if ident.get('type', '').lower() != 'doi' or 'id' not in ident:
                continue
            dois = extract_dois(ident['id'])
            if len(dois) > 0:
                doi = dois[0].lower()

        return {
            'idx': parsed['id'],
            'abstract': pre_process_text(parsed['bibjson'].get('abstract')),
            'month': int_or_none(parsed['bibjson'].get('month')),
            'title': pre_process_text(parsed['bibjson'].get('title')),
            'year': int_or_none(parsed['bibjson'].get('year')),
            'last_updated': parsed.get('last_updated'),
            'doi': doi,
            'journal_issns': [
                issn
                for issn in set(parsed['bibjson']['journal'].get('issns', []))
            ]
        }

    def download_unpack(self, url):
        """Download url, unpack files and return list of file names."""
        # download in chunks
        logger.debug(f'Start downloading {url}')
        r = requests.get(url, stream=True)
        chunk_size = 1000000
        filename = os.path.basename(r.url.split('?')[0])
        filename = Path(settings.DOWNLOAD_TEMP_DIR) / filename
        with open(filename, 'wb') as f:
            for chunk in r.iter_content(chunk_size=chunk_size):
                f.write(chunk)
        logger.debug(f'Download completed')

        # unpack *.tar.gz
        tar = tarfile.open(filename)
        tar.extractall(path=filename.parent)
        tar.close()
        os.remove(filename)
        logger.debug('Files unpacked')

        filename = Path(str(filename).removesuffix('.tar.gz'))
        return list(filename.rglob('*.json'))

    def insert_journals(self):
        """Iterate through journal batches and insert them."""
        url = 'https://doaj.org/public-data-dump/journal?api_key=' + settings.DOAJ_KEY
        files = self.download_unpack(url)
        logger.info('Updating DOAJ journals: START')

        with transaction.atomic():
            # Deleting everything and reinserting is faster than upserting.
            # Licenses are not deleted as the Extension refers to the ID as a
            # primary key which would get changed.
            Journal.objects.all().delete()
            Keyword.objects.all().delete()
            Subject.objects.all().delete()
            Language.objects.all().delete()
            IsReplacedBy.objects.all().delete()
            ApcMax.objects.all().delete()
            EditorialReviewProcess.objects.all().delete()
            data = []
            for filename in files:
                with open(filename) as f:
                    for line in f:
                        # parse one json object per line
                        line = line.strip()\
                                   .removeprefix('[')\
                                   .removesuffix(']')\
                                   .removesuffix(',')
                        parsed = json.loads(line)
                        data.append(self.parse_journal(parsed))

            # create journals
            journals = map(lambda e: Journal(**e['journal']), data)
            Journal.objects.bulk_create(journals, batch_size=1000)

            # use helper function to make it fast
            bulk_create_manytomany(Journal, Keyword, 'keywords', data)
            bulk_create_manytomany(Journal, EditorialReviewProcess,
                                   'editorial_review_process', data)
            bulk_create_manytomany(Journal, IsReplacedBy, 'is_replaced_by',
                                   data)
            bulk_create_manytomany(Journal, Subject, 'subjects', data)
            bulk_create_manytomany(Journal, Language, 'languages', data)
            bulk_create_manytomany(Journal, License, 'licenses', data)
            bulk_create_manytomany(Journal, ApcMax, 'apc_max', data)

        shutil.rmtree(files[0].parent)
        logger.info('Updating DOAJ journals: DONE')

    def parse_journal(self, parsed):
        return {
            'journal': {
                'idx': parsed['id'],
                'aims_scope': parsed['bibjson'].get('ref', {}).get('aims_scope'),
                'alternative_title': parsed['bibjson'].get('alternative_title'),
                'copyright_author_retains': parsed['bibjson'].get('copyright', {}).get('author_retains'),
                'created_date': parsed.get('created_date'),
                'discontinued_date': parsed['bibjson'].get('discontinued_date'),
                'doaj_seal': parsed['admin'].get('seal', False),
                'doi_pid_scheme': 'doi' in [scheme.lower() for scheme in parsed['bibjson'].get('pid_scheme', {}).get('scheme', [])],
                'editorial_board_url': parsed['bibjson'].get('editorial', {}).get('board_url'),
                'eissn': parsed['bibjson'].get('eissn'),
                'has_apc': parsed['bibjson'].get('apc', {}).get('has_apc'),
                'has_other_charges': parsed['bibjson'].get('other_charges', {}).get('has_other_charges'),
                'has_pid_scheme': parsed['bibjson'].get('pid_scheme', {}).get('has_pid_scheme'),
                'last_updated': parsed.get('last_updated'),
                'pissn': parsed['bibjson'].get('pissn'),
                'publication_time_weeks': parsed['bibjson'].get('publication_time_weeks'),
                'publisher_country': parsed['bibjson'].get('publisher', {}).get('country'),
                'publisher_name': parsed['bibjson'].get('publisher', {}).get('name'),
                'preservation_has_preservation': parsed['bibjson'].get('preservation', {}).get('has_preservation'),
                'ref_journal': parsed['bibjson'].get('ref', {}).get('journal'),
                'ref_author_instructions': parsed['bibjson'].get('ref', {}).get('author_instructions'),
                'title': parsed['bibjson'].get('title')
            },
            'editorial_review_process': [
                {'process': process}
                for process in parsed['bibjson'].get('editorial', {}).get('review_process', [])
            ],
            'keywords': [
                {'keyword': keyword}
                for keyword in set(parsed['bibjson'].get('keywords', []))
            ],
            'subjects': [
                {'code': subject['code'], 'scheme': subject['scheme'], 'term': subject['term']}
                for subject in uniq_dicts(parsed['bibjson'].get('subject', []))
            ],
            'languages': [
                {'lang': language}
                for language in set(parsed['bibjson'].get('language', []))
            ],
            'licenses': [{
                    'nc': license.get('NC'),
                    'nd': license.get('ND'),
                    'by': license.get('BY'),
                    'typex': license.get('type'),
                    'sa': license.get('SA')
                }
                for license in parsed['bibjson'].get('license', [])
            ],
            'apc_max': [{
                    'currency': apc['currency'],
                    'price': apc['price']
                }
                for apc in parsed['bibjson'].get('apc', {}).get('max', [])
            ],
            'is_replaced_by': [
                {'replacement': repl}
                for repl in parsed['bibjson'].get('is_replaced_by', [])
            ]
        }


def bulk_create_manytomany(db_parent_obj, db_obj, name, data):
    # insert elements one by one and map them to their pkey
    elements = {}
    for new_el in uniq_dicts(flatten(map(lambda e: e[name], data))):
        # frozenset items because normal dict cannot be key
        key = frozenset(new_el.items())
        elements[key] = db_obj.objects.get_or_create(**new_el)[0].pk

    # get through model and parameters for creating new object
    through_model = getattr(db_parent_obj, name).through
    table_name = db_obj.objects.model._meta.db_table
    id_name = table_name.removeprefix('recommender_') + '_id'
    parent_name = db_parent_obj.objects.model._meta.model_name

    # create the new through model objects and bulk insert them
    through_objects = []
    for row in data:
        for item in row[name]:
            # unpacked dict because one parameter name is dynamic
            through_obj = through_model(**{
                parent_name + '_id': row[parent_name]['idx'],
                id_name: elements[frozenset(item.items())]
            })
            through_objects.append(through_obj)
    through_model.objects.bulk_create(through_objects, batch_size=50000)
