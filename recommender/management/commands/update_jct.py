import itertools
import json
import logging
import time

from tqdm import tqdm
import requests
from django.core.management.base import BaseCommand
from django.db import transaction

from recommender.models import Journal, PlansCompliance, DatasetState


logger = logging.getLogger(__name__)


class Command(BaseCommand):
    help = 'Insert/Update the JournalCheckerTool data.'
    dataset = 'Journal Checker Tool'

    def handle(self, *args, **kwargs):
        DatasetState(dataset=self.dataset,
                     state=DatasetState.State.RUNNING).save()
        try:
            self.update()
            DatasetState(dataset=self.dataset,
                         state=DatasetState.State.UPDATED).save()
        except Exception:
            logger.exception('EXCEPTION WHEN UPDATING JCT DATA')
            DatasetState(dataset=self.dataset,
                         state=DatasetState.State.FAILED).save()

    def update(self):
        """Update the Plan-S compliance for all ISSNs in DOAJ."""
        logger.debug('Staring JCT update')
        # collect all ISSNs
        eissns = set(itertools.chain.from_iterable(
            Journal.objects.filter(eissn__isnull=False).values_list('eissn')
        ))
        pissns = set(itertools.chain.from_iterable(
            Journal.objects.filter(pissn__isnull=False).values_list('pissn')
        ))
        issns = list(eissns | pissns)

        # load compliance from journalcheckertool
        logger.debug(f'Requesting compliance for {len(issns)} issns')
        base_url = 'https://api.journalcheckertool.org/calculate?issn={}' +\
                   '&funder=europeancommissionhorizoneuropeframeworkprogramme'
        index = 0
        last_request = 0
        errors = 0
        with transaction.atomic(), tqdm(total=len(issns)) as pbar:
            PlansCompliance.objects.all().delete()
            db_objects = []

            while index < len(issns):
                # API as rate limit of 10 request per second
                if (diff := time.time() - last_request) <= 0.1:
                    time.sleep(diff)
                last_request = time.time()

                issn = issns[index]
                url = base_url.format(issn)
                try:
                    r = requests.get(url)
                    if r.status_code == 200:
                        index += 1
                        pbar.update(1)
                        compliant = json.loads(r.text)['compliant']
                        db_objects.append(
                            PlansCompliance(issn=issn, compliant=compliant)
                        )
                    elif r.status_code == 429:
                        # API limit exceeded
                        time.sleep(5)
                    else:
                        index += 1
                        pbar.update(1)
                        raise Exception(
                            f'JCT status code {r.status_code} for {issn}'
                        )
                except Exception as e:
                    logger.warning(
                        'Exception fetching infos from JCT: {}'.format(str(e))
                    )

                    errors += 1
                    if errors > 10:
                        # this is probably a bigger problem than just some
                        # wrong ISSNs
                        raise Exception('More than 10 errors fetching ISSNs')

            PlansCompliance.objects.bulk_create(db_objects)

        logger.debug('JCT update done')
