import logging

from django.core.management.base import BaseCommand
from django.db import connection
from django.db.utils import ProgrammingError

from recommender.models import DatasetState, DoajCitation


logger = logging.getLogger(__name__)


class Command(BaseCommand):
    help = 'Update or create the doaj citation materialized view.'
    dataset = 'DOAJ Citations'

    def handle(self, *args, **options):
        DatasetState(dataset=self.dataset,
                     state=DatasetState.State.RUNNING).save()
        logger.info('Update DOAJ Citations view')
        try:
            self.update()
            DatasetState(dataset=self.dataset,
                         state=DatasetState.State.UPDATED).save()
        except Exception:
            logger.exception('EXCEPTION WHEN UPDATING DOAJ CITATIONS VIEW')
            DatasetState(dataset=self.dataset,
                         state=DatasetState.State.FAILED).save()

    def update(self):
        try:
            DoajCitation.objects.first()
            logger.info('Refresh materialized view')
            with connection.cursor() as c:
                # This refresh locks the view. A concurrent refresh would be
                # possible if we had a unique index but it would probably
                # bloat the table size over time.
                c.execute("""
                    REFRESH MATERIALIZED VIEW
                    recommender_doaj_citation;
                """)
        except ProgrammingError:
            logger.info('Create new materialized view')
            with connection.cursor() as c:
                c.execute("""
                    CREATE MATERIALIZED VIEW recommender_doaj_citation AS
                    SELECT rc.id, articles.idx, issn, rc.frm, rc.tow
                    FROM (SELECT DISTINCT ON (doi) doi, issn, idx
                        FROM (SELECT idx, NULL as issn, doi
                              FROM recommender_article WHERE doi IS NOT NULL
                              UNION
                              SELECT NULL as idx, issn, doi
                              FROM recommender_crossrefarticle
                              ORDER BY doi, idx) alldois) articles
                    JOIN recommender_citation rc ON articles.doi = rc.frm;
                """)
                c.execute("""CREATE INDEX recommender_doaj_citation_tow
                             ON recommender_doaj_citation(tow);""")

        logger.info('Update DOAJ Citations view done')
