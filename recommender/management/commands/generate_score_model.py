import json
import logging
import os
from pathlib import Path
import pickle
import shutil

from django.core.management.base import BaseCommand
import numpy as np
import pytorch_lightning as pl
from sklearn.utils.class_weight import compute_class_weight
from sklearn.preprocessing import StandardScaler
import torch
from tqdm import tqdm

from recommender.search import recommend_journals
from recommender.models import Citation
from recommender.ml.scorer_model import ScorerModel


logger = logging.getLogger(__name__)


class Command(BaseCommand):
    help = 'Generate dataset and train scoring model.'
    base_path = Path('score_model')

    def add_arguments(self, parser):
        parser.add_argument('--generate_only', action='store_true')
        parser.add_argument('--train_only', action='store_true')

    def handle(self, *args, **options):
        if not os.path.exists(self.base_path):
            logger.info('Creating directory to for scorer training')
            os.mkdir(self.base_path)

        if not options['train_only']:
            logger.info('Generate scoring dataset')
            self.generate_dataset()

        if not options['generate_only']:
            logger.info('Train scoring model')
            self.train_model()

    def generate_dataset(self, test_file, size=20_000):
        """Generate train dataset of scores and whether they matched."""
        data = []
        searches = 0
        with tqdm(total=size) as pbar, open(test_file) as f:
            for line in f:
                if searches == size:
                    break

                article = json.loads(line)
                correct_journal = article['journal']

                if not article['doi']:
                    continue

                references = [
                    cit.tow
                    for cit in Citation.objects.filter(frm=article['doi'])
                ]
                if len(references) < 5:
                    continue
                references = ' '.join(references)

                searches += 1
                pbar.update(1)

                journals = recommend_journals(title=article['title'][:500],
                                              abstract=article['abstract'][:10000],
                                              references=references,
                                              skip_article=article['idx'])

                for journal in journals:
                    data.append(np.array([
                        journal['score']['title_score'],
                        journal['score']['abstract_score'],
                        journal['score']['dois_score'],
                        journal['score']['semantic_score'],
                        int(journal['idx'] == correct_journal)
                    ]))

        with open(self.base_path / 'score_dataset.npy', 'wb') as f:
            np.save(f, np.array(data))

    def train_model(self):
        """Train scorer model.

        The model receives title, abstract, reference and semantic neural net
        score and outputs the probability that the journal is the correct one.
        It only has two layers.
        """
        with open(self.base_path / 'score_dataset.npy', 'rb') as f:
            data = np.load(f)
        np.random.shuffle(data)

        # clip data to the 99,9% highest value
        col0 = np.sort(data[np.where(data[:, 0] > 0), 0])[0]
        col1 = np.sort(data[np.where(data[:, 1] > 0), 1])[0]
        col2 = np.sort(data[np.where(data[:, 2] > 0), 2])[0]
        col3 = np.sort(data[np.where(data[:, 3] > 0), 3])[0]
        data[:, 0] = np.clip(data[:, 0], 0, col0[int(col0.shape[0]*0.999)])
        data[:, 1] = np.clip(data[:, 1], 0, col1[int(col1.shape[0]*0.999)])
        data[:, 2] = np.clip(data[:, 2], 0, col2[int(col2.shape[0]*0.999)])
        data[:, 3] = np.clip(data[:, 3], 0, col3[int(col3.shape[0]*0.999)])

        # scale input so it does not range from zero to some thousands
        scaler = StandardScaler()

        # split between training and validation
        val_index = int(data.shape[0] * 0.01)

        X_train = data[val_index:, :4]
        X_train = scaler.fit_transform(X_train)
        y_train = data[val_index:, 4]

        X_val = data[:val_index, :4]
        X_val = scaler.transform(X_val)
        y_val = data[:val_index, 4]

        # use class weights as there are more negative than positive samples
        class_weights = compute_class_weight(
            'balanced',
            classes=np.unique(y_train),
            y=y_train
        )
        class_weights = torch.FloatTensor(class_weights)

        train_dataset = torch.utils.data.TensorDataset(torch.FloatTensor(X_train),
                                                       torch.LongTensor(y_train))
        val_dataset = torch.utils.data.TensorDataset(torch.FloatTensor(X_val),
                                                     torch.LongTensor(y_val))

        train_dataloader = torch.utils.data.DataLoader(train_dataset,
                                                       batch_size=50)
        val_dataloader = torch.utils.data.DataLoader(val_dataset,
                                                     batch_size=50)

        model = ScorerModel(class_weights)
        trainer = pl.Trainer(
            max_epochs=3,
            default_root_dir=self.base_path,
            gpus=1
        )
        trainer.fit(model, train_dataloader, val_dataloader)
        model.eval()

        # do some sanity checks of the scorer
        checks = model(torch.Tensor(scaler.transform([
            [0,   0,    0,   0.0],
            [10,  100,  1,   0.001],
            [100, 0,    0,   0.0],
            [0,   2000, 0,   0.0],
            [0,   0,    30,  0.0],
            [0,   0,    0,   0.2],
            [200, 3000, 100, 0.05],
        ])))[:, 1]
        if checks[0] > 0.3 or\
           checks[1] < 0.05 or checks[1] > 0.4 or\
           checks[2] < 0.20 or checks[2] > 0.95 or\
           checks[3] < 0.20 or checks[3] > 0.95 or\
           checks[4] < 0.20 or checks[4] > 0.95 or\
           checks[5] < 0.9 or\
           checks[6] < 0.9:
            logger.info(
                'Discarding model with dubious results {}'.format(checks)
            )
            shutil.rmtree(self.base_path)
            return

        # copy checkpoint and StandardScaler object and delete the rest
        shutil.move(
            list(self.base_path.rglob('*ckpt'))[-1],
            'recommender/ml/score_model/weights.ckpt'
        )

        with open('recommender/ml/score_model/scaler.pkl', 'wb') as f:
            pickle.dump(scaler, f)

        shutil.rmtree(self.base_path)
