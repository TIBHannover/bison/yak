import logging

from cachalot.api import cachalot_disabled
from django.core.management.base import BaseCommand
import django_elasticsearch_dsl.management.commands.search_index as es_dsl

from recommender.models import DatasetState


logger = logging.getLogger(__name__)


class Command(BaseCommand):
    help = 'Update/create the elasticsearch index.'
    dataset = 'Elasticsearch'

    def handle(self, *args, **options):
        DatasetState(dataset=self.dataset,
                     state=DatasetState.State.RUNNING).save()
        try:
            self.update()
            DatasetState(dataset=self.dataset,
                         state=DatasetState.State.UPDATED).save()
        except Exception:
            logger.exception('EXCEPTION WHEN UPDATING ELASTICSEARCH INDEX')
            DatasetState(dataset=self.dataset,
                         state=DatasetState.State.FAILED).save()

    def update(self):
        # Call the "rebuild --use-alias" command from django-elasticsearch-dsl.
        # The "use-alias" ensures that there should be zero downtime.
        logger.info('Indexing articles into Elasticsearch')
        with cachalot_disabled():
            c = es_dsl.Command()
            c.handle(action='rebuild', parallel=False, use_alias=True, 
                     models=[], force=False, use_alias_keep_index=False, 
                     count=False, refresh=False)
        logger.info('Indexing articles into Elasticsearch: DONE')
