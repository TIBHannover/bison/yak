from django.db import models
from django.utils import timezone


class DatasetState(models.Model):
    class State(models.TextChoices):
        FAILED = 'failed'
        UPDATED = 'updated'
        RUNNING = 'running'

    dataset = models.CharField(max_length=20)
    date = models.DateTimeField(default=timezone.now)
    state = models.CharField(choices=State.choices, max_length=20)
