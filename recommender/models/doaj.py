import logging

from django.db import models
from django.db.models import Q
from django.contrib.postgres.fields import ArrayField


logger = logging.getLogger(__name__)


# Some of these fields have an added "x" (e.g. typex) to avoid clashing with
# python keywords.


# DOAJ - article

class Article(models.Model):
    # Models articles from the DOAJ article dataset
    idx = models.CharField(primary_key=True, max_length=50)
    title = models.TextField(null=True)
    abstract = models.TextField(null=True)
    month = models.IntegerField(null=True)
    year = models.IntegerField(null=True)
    last_updated = models.DateTimeField()

    doi = models.CharField(null=True, db_index=True, max_length=300)
    journal_issns = ArrayField(models.CharField(max_length=100), blank=True)


class CrossrefArticle(models.Model):
    # Separate model for articles from Crossref. This could include articles
    # which where not uploaded to the DOAJ.
    issn = models.CharField(max_length=20, db_index=True)
    doi = models.CharField(max_length=300, db_index=True)


# DOAJ - journal

class EditorialReviewProcess(models.Model):
    process = models.CharField(primary_key=True, max_length=100)


class Keyword(models.Model):
    keyword = models.CharField(primary_key=True, max_length=2000)


class Subject(models.Model):
    code = models.CharField(primary_key=True, max_length=200)
    scheme = models.CharField(max_length=10)
    term = models.CharField(max_length=400)


class Language(models.Model):
    lang = models.CharField(primary_key=True, max_length=400)


class License(models.Model):
    nc = models.BooleanField(null=True)
    nd = models.BooleanField(null=True)
    by = models.BooleanField(null=True)
    sa = models.BooleanField(null=True)
    typex = models.CharField(null=True, max_length=100)

    def __str__(self):
        cc = []
        if self.by:
            cc.append('BY')
        if self.nc:
            cc.append('NC')
        if self.nd:
            cc.append('ND')
        if self.sa:
            cc.append('SA')
        return f'{self.typex} ({", ".join(cc)})'

    class Meta:
        ordering = ['typex']


class ApcMax(models.Model):
    price = models.IntegerField()
    currency = models.CharField(max_length=10)


class IsReplacedBy(models.Model):
    replacement = models.CharField(primary_key=True, max_length=300)


class Journal(models.Model):
    idx = models.CharField(primary_key=True, max_length=50)
    aims_scope = models.CharField(null=True, max_length=1000)
    alternative_title = models.TextField(null=True, max_length=1000)
    copyright_author_retains = models.BooleanField(null=True)
    created_date = models.DateTimeField()
    discontinued_date = models.DateTimeField(null=True)
    doaj_seal = models.BooleanField(default=False)
    doi_pid_scheme = models.BooleanField()
    editorial_board_url = models.CharField(null=True, max_length=1000)
    eissn = models.CharField(null=True, db_index=True, max_length=10)
    has_apc = models.BooleanField(null=True)
    has_other_charges = models.BooleanField(null=True)
    has_pid_scheme = models.BooleanField(null=True)
    last_updated = models.DateTimeField()
    pissn = models.CharField(null=True, db_index=True, max_length=10)
    publication_time_weeks = models.IntegerField(null=True)
    publisher_country = models.CharField(null=True, max_length=1000)
    publisher_name = models.CharField(null=True, max_length=1000)
    preservation_has_preservation = models.BooleanField(null=True)
    ref_journal = models.CharField(null=True, max_length=1000)
    ref_author_instructions = models.CharField(null=True, max_length=1000)
    title = models.CharField(null=True, max_length=1000)

    editorial_review_process = models.ManyToManyField(
        EditorialReviewProcess, related_name='journals')
    keywords = models.ManyToManyField(Keyword, related_name='journals')
    subjects = models.ManyToManyField(Subject, related_name='journals',)
    languages = models.ManyToManyField(Language, related_name='journals')
    licenses = models.ManyToManyField(License, related_name='journals')
    apc_max = models.ManyToManyField(ApcMax, related_name='journals')
    is_replaced_by = models.ManyToManyField(IsReplacedBy,
                                            related_name='journals')

    @staticmethod
    def get_journal(issns):
        """Get journal that matches the given ISSNs."""
        journal = None
        for issn in issns:
            try:
                journal = Journal.objects.get(Q(pissn=issn) |
                                              Q(eissn=issn))
                break
            except Exception:
                pass
        if journal is None:
            logger.warning('Journal not found for ISSN: {}'.format(issns))
        return journal
