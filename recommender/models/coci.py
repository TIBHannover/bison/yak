from django.db import models


class Citation(models.Model):
    # Modelling OpenCitations COCI Index.
    # A publication (from) is citing another publication (towards).
    frm = models.CharField(max_length=300)
    tow = models.CharField(max_length=300)

    def __str__(self):
        return f'Citation {self.frm} -> {self.tow}'


class DoajCitation(models.Model):
    """
    Materialized view with citations whose DOI is in the DOAJ.
    
    Used to speed up the lookup as this table contains less rows and already
    has the issn.
    """
    id = models.BigIntegerField(primary_key=True)
    issn = models.CharField(max_length=20)
    idx = models.CharField(max_length=50)  # article idx
    frm = models.CharField(max_length=300)
    tow = models.CharField(max_length=300)

    class Meta:
        managed = False
        db_table = 'recommender_doaj_citation'
