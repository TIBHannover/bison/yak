from django.db import models


class PlansCompliance(models.Model):
    issn = models.CharField(primary_key=True, max_length=20)
    compliant = models.BooleanField()

    def __str__(self):
        return f'Plan-S compliance for {self.issn}: {self.compliant}'

    @staticmethod
    def get_compliance(issn):
        '''Get whether issn is Plan-S compliant. Returns None if not found.'''
        try:
            return PlansCompliance.objects.get(issn=issn).compliant
        except models.DoesNotExist:
            return None
