import logging
import urllib.parse

from django.conf import settings
import requests

from recommender.models import Subject
from recommender.serializers import SubjectSerializer

"""
These are the functions that get predictions to further narrow down
the search results like the language or the subject.
"""


logger = logging.getLogger(__name__)


def predict_language(text):
    """Get language prediction from lingua  via http / ray serve."""
    try:
        response = requests.get(
            f'{settings.ML_SERVER}/Language',
            data=text.encode()
        )
        return response.json()['lang']
    except Exception:
        logger.exception('Exception while fetching language prediction')
        return None


def predict_subject(text):
    """Get subject prediction for text with ML model via http / ray serve."""
    try:
        response = requests.get(
            f'{settings.ML_SERVER}/Subject',
            data=text.encode()
        )
        subject_term = response.json()['subject']
        if subject_term is None:
            return None
        return SubjectSerializer(Subject.objects.get(term=subject_term)).data
    except Exception:
        logger.exception('Exception while fetching subject prediction')
        return None
