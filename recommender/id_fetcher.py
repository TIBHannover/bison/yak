import re

from bs4 import BeautifulSoup
import feedparser
from habanero import Crossref
import requests

from recommender.search import extract_dois


def fetch(input_id):
    """
    Fetch title, abstract and refernces for ID (DOI / arXiv ID).
    
    Raises IdNotFound exception.
    """
    dois = extract_dois(input_id)
    if len(dois) > 0:
        doi = dois[0]

        try:
            return fetch_crossref(doi)
        except IdNotFound:
            return fetch_datacite(doi)
    else:
        return fetch_arXiv(input_id)


def fetch_crossref(input_id):
    """Use crossref API to retrieve title, abstract and references for DOI."""
    cr = Crossref(mailto='bison@tib.eu', ua_string='BISON-DOI-fetcher')
    #TODO timeout?
    try:
        article = cr.works(ids=input_id)['message']
        references = ''
        if article['references-count'] > 0 and 'reference' in article:
            # Show references that have either a DOI or a title. The title is
            # not used but might put people at ease who are worried that some
            # of their references are missing.
            references = filter(lambda r: 'DOI' in r or 'article-title' in r,
                                article['reference'])
            references = '\n'.join(
                map(lambda r: f'{r.get("DOI", "")} {r.get("article-title", "")}',
                    references)
            )
        return {
            'title': article.get('title', [''])[0],
            'abstract': BeautifulSoup(article.get('abstract', ''),
                                      parser='lxml').text,
            'references': references
        }
    except requests.exceptions.HTTPError:
        raise IdNotFound(input_id)


def fetch_arXiv(input_id):
    """
    Use arXiv API to retrieve title and abstract for arXiv ID.
    
    References/Citations are not provided by the API. However, they could
    be extracted from the PDF.
    """
    fetch_arXiv.regex = getattr(fetch_arXiv, 'regex', None) or \
        re.compile(r"\d{4}.\d{4,6}(v\d+)?")
    try:
        arxiv_id = re.search(fetch_arXiv.regex, input_id).group()
    except Exception:
        raise IdNotFound(input_id)

    results = feedparser.parse(
        'http://export.arxiv.org/api/query?id_list=' + arxiv_id
    )
    if 'status' not in results or results.status != 200:
        raise IdNotFound(input_id)
    
    #TODO: Do title and summary always exist?
    return {
        'title': results.entries[0].title,
        'abstract': results.entries[0].summary,
        'references': ''
    }


def fetch_datacite(doi):
    """Use datacite API to retrieve title and abstract for DOI."""
    r = requests.get('https://api.datacite.org/dois/' + doi).json()
    if 'data' not in r:
        raise IdNotFound(doi)

    try:
        title = r['data']['attributes']['titles'][0]['title']
    except KeyError:
        title = None

    try:
        abstract = r['data']['attributes']['descriptions'][0]['description']
    except KeyError:
        abstract = None

    #TODO: are there references in DataCite metadata?
    return {'title': title, 'abstract': abstract, 'references': ''}


class IdNotFound(Exception):
    pass
