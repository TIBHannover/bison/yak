import logging
import re

from django.conf import settings
from django.db.models import Count
import requests

from recommender.documents import ArticleDocument
from recommender.models import Article, Journal, DoajCitation, DatasetState
from recommender.serializers import JournalSerializer

logger = logging.getLogger(__name__)


def extract_dois(text):
    """Extract a list of DOIs from a string based on regexs."""
    # These regexs supposedly match > 99% of DOIs.
    # See: https://www.crossref.org/blog/dois-and-matching-regular-expressions/
    # Compiled as "static" variable for more efficiency
    extract_dois.regex1 = getattr(extract_dois, 'regex1', None) or \
        re.compile(r"10.\d{4,9}/[-._;()/:A-Z0-9]+", flags=re.IGNORECASE)
    extract_dois.regex2 = getattr(extract_dois, 'regex2', None) or \
        re.compile(r"10.1002/[^\s]+", flags=re.IGNORECASE)

    return re.findall(extract_dois.regex1, text) +\
           re.findall(extract_dois.regex2, text)


def classify_references(refs, skip_article=None):
    """Get list of journals with articles citing/cited by DOIs in refs."""
    # Test citation which is in DOAJ and OpenCitations:
    # doi:10.3390/publications7010006 -> doi:10.12688/f1000research.11408.1

    if refs == '' or refs is None:
        return []

    state_query = (DatasetState.objects.filter(dataset='DOAJ Citations')
                                       .order_by('-date'))
    if state_query.exists() and state_query.first().state == DatasetState.State.RUNNING:
        # The doaj citation view is locked during update. However, the update
        # should take under 20min.
        logger.warning('Skipping reference search as DB view is blocked')
        return []

    dois = extract_dois(refs)[:200]

    # find articles citing the input references
    citing_arts = DoajCitation.objects.filter(tow__in=dois)\
                                      .values('idx', 'frm', 'issn')\
                                      .annotate(total=Count('frm'))
    if skip_article:
        citing_arts = list(filter(lambda a: a['idx'] != skip_article,
                                  citing_arts))
    if len(citing_arts) == 0:
        return []
    max_citations = max(citing_arts, key=lambda e: e['total'])['total']
    citing_arts = filter(lambda e: e['total']/max_citations >= 1/8,
                          citing_arts)

    results = []
    for cite in citing_arts:
        title = cite['frm']
        issns = [cite['issn']]
        if cite['idx'] is not None:
            try:
                article = Article.objects.get(idx=cite['idx'])
                title = article.title or title
                issns = article.journal_issns
            except Article.DoesNotExist:
                logger.warning(f'Article {cite["idx"]} does not exist')

        results.append({
            'idx': cite['idx'],
            'journal_issns': issns,
            'doi': cite['frm'],
            'title': cite['frm'],
            'score': cite['total']
        })

    return results


def classify_es_match(abstract):
    """Classify abstract based only elastic text match."""
    if abstract == '' or abstract is None:
        return []
    abstract = abstract[:4500]  # otherwise the query is too long
    search = ArticleDocument.search()\
        .query('match', abstract=abstract)\
        .extra(min_score=100)\
        .params(size=100, request_timeout=40)
    results = search.execute()
    for result in results:
        result.score = result.meta.score

    return list(results)


def classify_es_title_match(title):
    """Classify title based on elastic text match of other titles."""
    if title == '' or title is None:
        return []
    search = ArticleDocument.search()\
        .query('match', title=title)\
        .extra(min_score=15)\
        .params(size=100)
    results = search.execute()
    for result in results:
        result.score = result.meta.score

    return list(results)


def classify_semantic_nn(text):
    """Classify journals based on text using neural network."""
    ml_result = requests.get(
        f'{settings.ML_SERVER}/Semantic',
        data=text.encode()
    )
    return ml_result.json()


def recommend_journals(title=None, abstract=None, references=None,
                       skip_article=None):
    """THE recommender function returning journals based on title etc."""
    abstract_articles = classify_es_match(abstract)
    title_articles = classify_es_title_match(title)
    references_articles = classify_references(references,
                                              skip_article=skip_article)
    ml_result = classify_semantic_nn((title or '') + ' ' + (abstract or ''))

    # find journals and add score
    journals = {}
    journals = get_journals_for_articles(journals, title_articles, 'title', skip_article)
    journals = get_journals_for_articles(journals, abstract_articles, 'abstract', skip_article)
    journals = get_journals_for_articles(journals, references_articles, 'dois', skip_article)
    journals = get_journals_for_semantic_nn(journals, ml_result)

    journals = list(journals.values())
    journals = score(journals, skip_article=skip_article)
    journals.sort(key=lambda e: e.score['value'], reverse=True)

    return JournalSerializer(journals, many=True).data


def get_journals_for_articles(journals, articles, source, skip_article=None):
    """Match articles to their journal"""
    for article in articles:
        if article['idx'] == skip_article:
            # this is for generating data sets or evaluating predictions
            continue

        journal = Journal.get_journal(article['journal_issns'])
        if journal is None:
            continue

        # filter out journals that are discontinued or replace with successor
        if journal.discontinued_date is not None:
            if journal.is_replaced_by.exists():
                journal = Journal.get_journal(
                    [journal.is_replaced_by.first().replacement]
                )
                if journal is None:
                    continue
            else:
                continue

        journal = journals.get(journal.idx, journal)
        journals[journal.idx] = journal
        if hasattr(journal, 'score') is False:
            journal.score = {
                'title': [],
                'abstract': [],
                'dois': [],
                'title_score': 0,
                'abstract_score': 0,
                'dois_score': 0,
                'semantic_score': 0
            }

        journal.score[source].append({
            'title': article['title'],
            'doi': article['doi'] if 'doi' in article else None,
        })
        journal.score[source + '_score'] += article['score']
    return journals


def get_journals_for_semantic_nn(journals, ml_result):
    """Convert results from semantic neural net to journals."""
    for idx, score in zip(ml_result['journals'], ml_result['probabilities']):
        if idx in journals:
            journals[idx].score['semantic_score'] = score
        else:
            try:
                journal = Journal.objects.get(idx=idx)
            except Journal.DoesNotExist:
                # this could happen if there are inconsistency in the data set
                continue

            journal.score = {
                'title': [],
                'abstract': [],
                'dois': [],
                'title_score': 0,
                'abstract_score': 0,
                'dois_score': 0,
                'semantic_score': score
            }
            journals[idx] = journal
    return journals


def score(journals, skip_article=None):
    """Calculate journal scores using neural network."""
    # create array so the scoring model can evaluate all at once
    scores = []
    for journal in journals:
        scores.append([
            journal.score['title_score'],
            journal.score['abstract_score'],
            journal.score['dois_score'],
            journal.score['semantic_score']
        ])
        if skip_article is None:
            journal.score.pop('title_score'),
            journal.score.pop('abstract_score'),
            journal.score.pop('dois_score'),
            #journal.score.pop('semantic_score')

    response = requests.post(
        f'{settings.ML_SERVER}/Scorer',
        json={'scores': scores}
    )
    scores = response.json()
    for i, journal in enumerate(journals):
        journal.score['value'] = float(scores[i])

    return journals
