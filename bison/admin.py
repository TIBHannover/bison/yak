from django.contrib.admin import AdminSite
from django.contrib.admin.apps import AdminConfig


class BISONAdminSite(AdminSite):
    site_header = "B!SON admin"
    site_title = "B!SON admin portal"
    index_title = "Welcome to the B!SON admin portal"


class BISONAdminConfig(AdminConfig):
    default_site = 'bison.admin.BISONAdminSite'
