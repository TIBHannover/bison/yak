from django.urls import path, include
from django.contrib import admin

urlpatterns = [
    path('bison/', include('api.urls')),
    path('bison/bison_admin/', admin.site.urls),
]
