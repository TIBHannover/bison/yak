from configurations.values import Value, ListValue, SingleNestedTupleValue
from .general import General


class Production(General):
    DEBUG = False
    SECRET_KEY = Value()
    GITLAB_ISSUE_SECRET = Value()
    ALLOWED_HOSTS = ListValue(default=['localhost'])
    CSRF_TRUSTED_ORIGINS = ListValue()
    CACHES = {
        'default': {
            'BACKEND': 'django.core.cache.backends.memcached.PyMemcacheCache',
            'LOCATION': '{}:11211'.format(
                Value(environ_name='CACHE_HOST', default='localhost')),
            'TIMEOUT': 60*60*12  # 12 hours
        }
    }

    LOGGING = {
        'version': 1,
        'disable_existing_loggers': True,
        'formatters': {
            'verbose': {
                'format': '[%(asctime)s][%(levelname)s][%(name)s.%(funcName)s:%(lineno)d] %(message)s',
                'datefmt': '%Y-%m-%d %H:%M:%S'
            }
        },
        'handlers': {
            'production_log': {
                'class': 'logging.handlers.RotatingFileHandler',
                'maxBytes': 1024 * 1024 * 50,  # 50MB
                'backupCount': 3,
                'filename': '/var/log/django/django.log',
                'formatter': 'verbose',
            },
            'mail_admins': {
                'level': 'ERROR',
                'class': 'django.utils.log.AdminEmailHandler',
            }
        },
        'root': {
            'handlers': ['production_log', 'mail_admins'],
            'level': 'WARNING',
        },
        'loggers': {
            'api': {
                'handlers': ['production_log', 'mail_admins'],
                'level': 'INFO',
                'propagate': False
            },
            'django': {
                'handlers': ['production_log', 'mail_admins'],
                'level': 'INFO',
                'propagate': False
            },
            'recommender.management': {
                'handlers': ['production_log', 'mail_admins'],
                'level': 'DEBUG',
                'propagate': False
            }
        }
    }

    # Mail reporting
    ADMINS = SingleNestedTupleValue()
    EMAIL_SUBJECT_PREFIX = 'B!SON [Django] '

    DATABASES = {
        'default': {
            'ENGINE': 'django.db.backends.postgresql_psycopg2',
            'NAME': 'bison',
            'USER': 'bison',
            'PASSWORD': Value(environ_name='DB_PASSWORD'),
            'HOST': Value(environ_name='DB_HOST', default='postgres'),
            'PORT': '5432',
        }
    }

    ELASTICSEARCH_DSL = {
        'default': {
            'hosts': 'elasticsearch://{user}:{password}@{host}:{port}'.format(
                user=Value(environ_name='ES_USER', default='elastic'),
                password=Value(environ_name='ES_PASSWORD'),
                host=Value(environ_name='ES_HOST', default='elasticsearch'),
                port='9200'
            )
        },
    }

    DOWNLOAD_TEMP_DIR = Value()
    ES_STOPWORDS_PATH = Value()
    ML_SERVER = Value()
    DOAJ_KEY = Value()

    # axes configuration for protection of django admin login
    AXES_IPWARE_META_PRECEDENCE_ORDER = [
        'HTTP_X_FORWARDED_FOR',
        'REMOTE_ADDR',
    ]
    AXES_IPWARE_PROXY_COUNT = 1
    AXES_FAILURE_LIMIT = 5
    AXES_COOLOFF_TIME = 2  # in hours

    # limits the amount of objects that can be deleted in django-admin
    # in one batch
    DATA_UPLOAD_MAX_NUMBER_FIELDS = 5000
