from .general import General


class Dev(General):
    SECRET_KEY = 'django-insecure-oj*3^^sjl05*eol2ej*_phe#p9()h#r#*0w4q%rmj!2o$60ga+'
    DEBUG = True
    ALLOWED_HOSTS = [
        'localhost',
        '127.0.0.1'
    ]
    INTERNAL_IPS = ALLOWED_HOSTS

    MEDIA_ROOT = General.BASE_DIR / 'media'
    STATIC_ROOT = General.BASE_DIR / 'static'
