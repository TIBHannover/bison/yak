from pathlib import Path
from configurations import Configuration


class General(Configuration):
    # Build paths inside the project like this: BASE_DIR / 'subdir'.
    BASE_DIR = Path(__file__).resolve().parent.parent.parent

    LOGGING = {
        'version': 1,
        'disable_existing_loggers': True,
        'formatters': {
            'verbose': {
                'format': '[%(asctime)s][%(levelname)s][%(name)s.%(funcName)s:%(lineno)d] %(message)s',
                'datefmt': '%Y-%m-%d %H:%M:%S'
            }
        },
        'handlers': {
            'console': {
                'level': 'DEBUG',
                'class': 'logging.StreamHandler',
                'formatter': 'verbose',
            },
        },
        'root': {
            'handlers': ['console'],
            'level': 'INFO',
        },
        'loggers': {
            'api': {
                'handlers': ['console'],
                'level': 'DEBUG',
                'propagate': False
            },
            'django': {
                'handlers': ['console'],
                'level': 'INFO',
                'propagate': False
            },
            'recommender': {
                'handlers': ['console'],
                'level': 'INFO',
                'propagate': False
            },
            'recommender.management': {
                'handlers': ['console'],
                'level': 'DEBUG',
                'propagate': False
            }
        }
    }


    # Application definition

    INSTALLED_APPS = [
        'modeltranslation',
        'bison.admin.BISONAdminConfig',
        'django.contrib.auth',
        'django.contrib.contenttypes',
        'django.contrib.sessions',
        'django.contrib.messages',
        'django.contrib.staticfiles',
        'api',
        'recommender',
        'extension',
        'rest_framework',
        'drf_spectacular',
        'django_elasticsearch_dsl',
        'cachalot',
        'import_export',
        'axes'
    ]

    # django-elasticsearch-dsl

    ELASTICSEARCH_DSL = {
        'default': {
            'hosts': 'localhost:9200'
        },
    }
    ELASTICSEARCH_DSL_PARALLEL = True

    # rest-framework and drf-spectacular
    REST_FRAMEWORK = {
        'DEFAULT_SCHEMA_CLASS': 'drf_spectacular.openapi.AutoSchema',
        'DEFAULT_AUTHENTICATION_CLASSES': [],
    }
    SPECTACULAR_SETTINGS = {
        # spec: https://github.com/OAI/OpenAPI-Specification/blob/master/versions/3.0.3.md#openapi-object
        'TITLE': 'B!SON API',
        'DESCRIPTION': 'B!SON API to retrieve recommendations for open-access journal in a machine-readable format.',
        'TOS': None,
        'CONTACT': {'email': 'bison@tib.eu'},
        # Optional: MUST contain "name", MAY contain URL
        'LICENSE': {},
        'VERSION': '0.0.1',
        # Tags defined in the global scope
        'TAGS': [],
    }

    MIDDLEWARE = [
        'django.middleware.security.SecurityMiddleware',
        'django.contrib.sessions.middleware.SessionMiddleware',
        'django.middleware.locale.LocaleMiddleware',
        'django.middleware.common.CommonMiddleware',
        'django.middleware.csrf.CsrfViewMiddleware',
        'django.contrib.auth.middleware.AuthenticationMiddleware',
        'django.contrib.messages.middleware.MessageMiddleware',
        'django.middleware.clickjacking.XFrameOptionsMiddleware',
        'axes.middleware.AxesMiddleware',
    ]
    ROOT_URLCONF = 'bison.urls'
    TEMPLATES = [
        {
            'BACKEND': 'django.template.backends.django.DjangoTemplates',
            'DIRS': [Path(BASE_DIR, 'templates')],
            'APP_DIRS': True,
            'OPTIONS': {
                'context_processors': [
                    'django.template.context_processors.debug',
                    'django.template.context_processors.request',
                    'django.contrib.auth.context_processors.auth',
                    'django.contrib.messages.context_processors.messages',
                ],
            },
        },
    ]
    WSGI_APPLICATION = 'bison.wsgi.application'


    # Database
    # https://docs.djangoproject.com/en/3.2/ref/settings/#databases

    DATABASES = {
        'default': {
            'ENGINE': 'django.db.backends.postgresql_psycopg2',
            'NAME': '',
            'USER': '',
            'PASSWORD': '',
            'HOST': '',
            'PORT': '',
        }
    }


    # Password validation
    # https://docs.djangoproject.com/en/3.2/ref/settings/#auth-password-validators

    AUTH_PASSWORD_VALIDATORS = [
        {'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator'},
        {'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator'},
        {'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator'},
        {'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator'},
    ]

    AUTHENTICATION_BACKENDS = [
        'axes.backends.AxesStandaloneBackend',
        'django.contrib.auth.backends.ModelBackend',
    ]

    # Internationalization
    # https://docs.djangoproject.com/en/3.2/topics/i18n/

    LANGUAGE_CODE = 'de'
    TIME_ZONE = 'UTC'
    USE_I18N = True
    USE_L10N = True
    USE_TZ = True

    LANGUAGES = [
        ("de", "German"),
        ("en", "English"),
    ]

    # Static files (CSS, JavaScript, Images)
    # https://docs.djangoproject.com/en/3.2/howto/static-files/

    # STATICFILES_DIRS = [Path(BASE_DIR, 'static')]  -> currently not used
    STATIC_URL = '/bison/static/'
    STATIC_ROOT = '/var/www/bison/static'

    MEDIA_ROOT = '/var/www/bison/media'
    MEDIA_URL = '/bison/media/'


    # Default primary key field type
    # https://docs.djangoproject.com/en/3.2/ref/settings/#default-auto-field

    DEFAULT_AUTO_FIELD = 'django.db.models.BigAutoField'

    GITLAB_ISSUE_SECRET = None

    # Caching
    # https://docs.djangoproject.com/en/4.0/topics/cache/
    CACHES = {
        'default': {
            'BACKEND': 'django.core.cache.backends.locmem.LocMemCache',
            'OPTIONS': {
                'MAX_ENTRIES': 10000
            }
        }
    }
    CACHALOT_TIMEOUT = 60*60*24  # 24 hours
    CACHALOT_ENABLED = True
    CACHALOT_UNCACHABLE_TABLES = frozenset(('django_migrations', 'recommender_datasetstate'))

    # directory for temporarily saving COCI, DOAJ,...
    DOWNLOAD_TEMP_DIR = ''
    ES_STOPWORDS_PATH = "/etc/elasticsearch/stopwords.txt"

    MAX_JOURNAL_LIMIT = 100  # max number of journals returned

    # port changes for the ML_SERVER, require a change in serve_ml.py as well
    ML_SERVER = 'http://localhost:8012'

    # DOAJ now requires an API key to download the data dumps
    DOAJ_KEY = ''
