from configurations.values import Value
from .general import General


class SecretPodmanValue(Value):
    def setup(self, name):
        value = self.default
        with open('/run/secrets/' + name, 'r') as f:
            value = f.readline().strip()
        self.value = value
        return value


class Staging(General):
    DEBUG = False
    SECRET_KEY = SecretPodmanValue()
    GITLAB_ISSUE_SECRET = SecretPodmanValue()
    ALLOWED_HOSTS = [
        'localhost',
        '10.114.149.44',
        'bison21.service.tib.eu'
    ]
    CACHES = {
        'default': {
            'BACKEND': 'django.core.cache.backends.memcached.PyMemcacheCache',
            'LOCATION': 'localhost:11211',
            'TIMEOUT': 60*60*12  # 12 hours
        }
    }

    LOGGING = {
        'version': 1,
        'disable_existing_loggers': True,
        'formatters': {
            'verbose': {
                'format': '[%(asctime)s][%(levelname)s][%(name)s.%(funcName)s:%(lineno)d] %(message)s',
                'datefmt': '%Y-%m-%d %H:%M:%S'
            }
        },
        'handlers': {
            'production_log': {
                'level': 'WARNING',
                'class': 'logging.handlers.RotatingFileHandler',
                'maxBytes': 1024 * 1024 * 50,  # 50MB
                'backupCount': 3,
                'filename': '/logs/django.log',
                'formatter': 'verbose',
            }
        },
        'root': {
            'handlers': ['production_log'],
            'level': 'WARNING',
        },
        'loggers': {
            'api': {
                'handlers': ['production_log'],
                'level': 'INFO',
                'propagate': False
            },
            'django': {
                'handlers': ['production_log'],
                'level': 'INFO',
                'propagate': False
            }
        }
    }
    DATASET_UPDATE_INTERVAL_DAYS = 21
