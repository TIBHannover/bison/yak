from django.urls import path

from . import api_views
from drf_spectacular.views import SpectacularAPIView, SpectacularSwaggerView


urlpatterns = [
    path('api/public/v1/journal/<str:pk>', api_views.JournalDetail.as_view()),
    path('api/public/v1/search', api_views.search, name='api-search'),
    path('api/public/v1/status', api_views.status),
    path('api/feedback', api_views.create_feedback, name='api-feedback'),
    path('api/news', api_views.get_news),
    path('api/fetchdoi/', api_views.fetch_doi),
    path('api/extension/<int:pk>', api_views.GetExtension.as_view()),
    # OpenAPI and swagger
    path('api/schema', SpectacularAPIView.as_view(), name='schema'),
    path(
        'api/schema/swagger',
        SpectacularSwaggerView.as_view(url_name='schema'),
        name='swagger-ui',
    ),
]
