import itertools
import logging
from urllib.parse import urljoin

from django.conf import settings
from django.db.models import Q
from django.utils.decorators import method_decorator
from django.views.decorators.cache import cache_page
from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework.parsers import JSONParser
from rest_framework.throttling import AnonRateThrottle
from rest_framework.decorators import parser_classes, throttle_classes
from rest_framework.status import (HTTP_200_OK, HTTP_400_BAD_REQUEST,
                                   HTTP_404_NOT_FOUND,
                                   HTTP_500_INTERNAL_SERVER_ERROR,
                                   HTTP_503_SERVICE_UNAVAILABLE)
from drf_spectacular.utils import (extend_schema, OpenApiParameter,
                                   OpenApiExample, inline_serializer)
from rest_framework import mixins, generics, serializers
import requests
from bs4 import BeautifulSoup

from extension.models import Extension
from extension.serializers import ExtensionSerializer
from recommender import id_fetcher
from recommender.models import Journal, Subject, DatasetState
from recommender.pre_filter import predict_language, predict_subject
from recommender.search import recommend_journals
from recommender.serializers import (JournalSerializer, SearchSerializer,
                                     SubjectSerializer)


logger = logging.getLogger(__name__)


def _try_lambda(l):
    """
    Wrapper for DB calls in decorator to catch exceptions.

    This mainly happens when the database is empty.
    """
    try:
        return l(None)
    except Exception as e:
        return None


class JournalDetail(mixins.RetrieveModelMixin, generics.GenericAPIView):
    """Get detailed information about a journal."""

    queryset = Journal.objects.all()
    serializer_class = JournalSerializer

    @extend_schema(
        examples=[
            OpenApiExample(
                name='Human-Wildlife Interactions',
                value='9135cb8e420946e3a66cfdf74dd15f08',
                parameter_only=('idx', OpenApiParameter.PATH)
            ),
            OpenApiExample(
                name='Human-Wildlife Interactions',
                value=_try_lambda(
                    lambda x: JournalSerializer(
                        Journal.objects.get(idx='9135cb8e420946e3a66cfdf74dd15f08')
                    ).data
                )
            ),
        ]
    )
    @method_decorator(cache_page(60*60*12))
    def get(self, request, *args, **kwargs):
        return self.retrieve(request, *args, **kwargs)


@extend_schema(
    examples=[
        OpenApiExample(
            'Example "django" search',
            value={
                'language': 'EN',
                'subject': _try_lambda(
                    lambda x: SubjectSerializer(Subject.objects.first()).data
                ),
                'journals': _try_lambda(
                    lambda x: recommend_journals(title='Django')[:2]
                )
            },
            response_only=True
        ),
        OpenApiExample(
            'Example user input',
            value={
                'title': 'Pushing the button: Why do learners pause online…',
                'abstract': 'With the recent surge in digitalization across…',
                'references': 'Ayres and Paas, 2007 10.1002/acp.1343 …'
            },
            request_only=True
        ),
    ],
    request=SearchSerializer,
    responses=inline_serializer(
        name='RecommenderResultSerializer',
        fields={
            'language': serializers.CharField(),
            'subject': SubjectSerializer(),
            'journals': JournalSerializer(many=True)
        }
    )
)
@api_view(['POST'])
@parser_classes([JSONParser])
def search(request):
    """Search for journals based on title, abstract,... of manuscript."""
    limit = 50000  # set a limit for security
    title = (request.data.get('title') or '')[:limit]
    abstract = (request.data.get('abstract') or '')[:limit]
    references = (request.data.get('references') or '')[:limit]

    text = title + ' ' + abstract
    lang = predict_language(text)
    subject = predict_subject(text)

    # fetch journal recommendations
    try:
        results = recommend_journals(
            title=title,
            abstract=abstract,
            references=references
        )
        results = results[:settings.MAX_JOURNAL_LIMIT]
        # check if predicted language appears in languages of recommendations
        journals_languages = itertools.chain.from_iterable(
            map(lambda j: j.get('languages', []), results)
        )
        if lang and lang not in journals_languages:
            lang = None

        return Response({
            'journals': results,
            'language': lang,
            'subject': subject
        })
    except Exception:
        logger.exception('Exception while fetching recommendations')
        return Response(status=HTTP_500_INTERNAL_SERVER_ERROR)


class FeedbackRateThrottle(AnonRateThrottle):
    rate = '50/day'


@extend_schema(exclude=True)
@api_view(['POST'])
@throttle_classes([FeedbackRateThrottle])
@parser_classes([JSONParser])
def create_feedback(request):
    """Send feedback as new issue to gitlab."""
    if settings.GITLAB_ISSUE_SECRET is None:
        return Response(status=HTTP_500_INTERNAL_SERVER_ERROR)

    headers = {'PRIVATE-TOKEN': settings.GITLAB_ISSUE_SECRET}
    url = 'https://git.tib.eu/api/v4/projects/1444/issues?labels=feedback&title={}&description={}'
    if 'text' not in request.data:
        return Response(status=HTTP_400_BAD_REQUEST)
    text = request.data['text']
    if len(text.strip()) < 5:
        # drop empty/short feedback
        return Response(status=HTTP_200_OK)

    title = 'Feedback:+' + text[:50] + '...'
    text = text[:2000]
    url = url.format(title, text)

    r = requests.post(url, headers=headers)
    if r.status_code == 201:
        return Response(status=HTTP_200_OK)
    else:
        return Response(status=HTTP_500_INTERNAL_SERVER_ERROR)


@extend_schema(exclude=True)
@api_view(['GET'])
@cache_page(60*60*12)
def get_news(request):
    """Get list of project news."""
    r = requests.get('https://projects.tib.eu/bison/en/news/')
    if r.status_code != 200:
        return Response(status=HTTP_503_SERVICE_UNAVAILABLE)

    r = BeautifulSoup(r.content, features='lxml')
    news = []
    articles = r.findAll('div', {'itemtype': 'http://schema.org/Article'})
    for article in articles[:5]:
        news.append({
            'title': article.find('h4').text.strip(),
            'teaser': article.find('div', {'class': 'teaser-text'}).text.strip(),
            'url': urljoin('https://projects.tib.eu/',
                           article.find('a').attrs['href'])
        })

    return Response(news)


@extend_schema(exclude=True)
@api_view(['GET'])
@cache_page(60*60*12)
def fetch_doi(request):
    """Fetch title, abstract, references from crossref/datacite/arXiv."""
    doi = request.GET.get('doi', '')
    if len(doi) < 5 or len(doi) > 300:
        return Response(status=HTTP_400_BAD_REQUEST)

    try:
        return Response(id_fetcher.fetch(doi))
    except id_fetcher.IdNotFound:
        return Response(status=HTTP_404_NOT_FOUND)
    except Exception as e:
        return Response(status=HTTP_500_INTERNAL_SERVER_ERROR)


@extend_schema(
    examples=[
        OpenApiExample(
            'Example status',
            value={'OpenCitations Meta': '2022-01-20T00:00:00Z'},
            response_only=True
        ),
    ],
    responses=inline_serializer(
        name='StatesSerializer',
        fields={'<datasetname>': serializers.CharField()}
    ),
)
@api_view(['GET'])
def status(request):
    """
    Get information on dataset status of COCI, DOAJ and JCT.

    Returns the last date of a successful update.
    """
    states = {}
    for dataset in ['OpenCitations Meta', 'DOAJ', 'Journal Checker Tool']:
        update_date = None
        update_query = DatasetState.objects.filter(
            Q(dataset=dataset) & Q(state=DatasetState.State.UPDATED)
        )
        if update_query.exists():
            update_date = update_query.order_by('-date').first().date
        states[dataset] = update_date
    return Response(states)


@extend_schema(exclude=True)
class GetExtension(mixins.RetrieveModelMixin, generics.GenericAPIView):
    """Get detailed information about an extension via ID."""

    queryset = Extension.objects.all()
    serializer_class = ExtensionSerializer

    def get(self, request, *args, **kwargs):
        return self.retrieve(request, *args, **kwargs)
