from django.test import TestCase, Client
from django.test.runner import DiscoverRunner


"""
As it is difficult to test the API without Elasticsearch etc., run the tests
with the equipped database via:
$ python manage.py test api --testrunner=api.tests.NoDbTestRunner
"""


class NoDbTestRunner(DiscoverRunner):
    """A test runner to test without database creation/deletion"""
    def setup_databases(self, **kwargs):
        pass

    def teardown_databases(self, old_config, **kwargs):
        pass


class TestAPI(TestCase):
    def setUp(self):
        self.client = Client()

    def test_search(self):
        response = self.client.post(
            '/bison/api/public/v1/search',
            {'title': 'cnn image recognition'},
            'application/json',
        )
        self.assertEqual(response.status_code, 200)
        self.assertGreaterEqual(len(response.json()['journals']), 1)
        self.assertIn('score', response.json()['journals'][0])
        self.assertIn('language', response.json())
        self.assertIn('subject', response.json())

    def test_search_empty(self):
        response = self.client.post(
            '/bison/api/public/v1/search',
            {'abstract': '', 'title': ''},
            'application/json',
        )
        self.assertEqual(response.status_code, 200)
        self.assertGreaterEqual(len(response.json()['journals']), 0)

    def test_journal(self):
        response = self.client.get(
            '/bison/api/public/v1/journal/2fdf1470373343b7bd4f825179c685f5'
        )
        self.assertEqual(response.status_code, 200)
        self.assertIn('title', response.json())

    def test_journal_notfound(self):
        response = self.client.get(
            '/bison/api/journal/xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx'
        )
        self.assertEqual(response.status_code, 404)

    def test_news(self):
        response = self.client.get('/bison/api/news')
        self.assertGreaterEqual(len(response.json()), 1)
        self.assertEqual(list(response.json()[0].keys()), ['title', 'teaser', 'url'])

    def test_doi(self):
        response = self.client.get(
            '/bison/api/fetchdoi/?doi=10.1007%2Fs00799-021-00306-x'
        )
        self.assertEqual(response.status_code, 200)
        result = response.json()
        self.assertIn('title', result)
        self.assertIn('abstract', result)
        self.assertIn('references', result)
        self.assertGreater(len(result['title']), 0)

    def test_status(self):
        response = self.client.get('/bison/api/public/v1/status')
        self.assertEqual(response.status_code, 200)
        result = response.json()
        self.assertIn('OpenCitations Meta', result)
