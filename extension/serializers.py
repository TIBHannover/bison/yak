from django.conf import settings
from modeltranslation.utils import (get_translation_fields,
                                    build_localized_fieldname)
from rest_framework import serializers

from recommender.serializers import LicenseSerializer
from .models import Extension
from .translation import ExtensionTranslationOptions


class TranslationSerializer(serializers.ModelSerializer):
    """ Serializer for the translated fields defined in `translation.py`."""

    def __init__(self, lang, *args, **kwargs):
        super().__init__(*args, **kwargs)

        translated = [
            build_localized_fieldname(field, lang)
            for field in ExtensionTranslationOptions.fields
        ]
        for field in set(self.fields):
            if field not in translated:
                self.fields.pop(field)

    class Meta:
        model = Extension
        fields = '__all__'


class ExtensionSerializer(serializers.ModelSerializer):
    licenses = LicenseSerializer(many=True)
    mirror_journals = serializers.SerializerMethodField()
    licensed_journals = serializers.SerializerMethodField()
    institution_logo = serializers.SerializerMethodField()
    translations = serializers.SerializerMethodField()

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        # remove translated fields
        fields_translated = []
        for field in ExtensionTranslationOptions.fields:
            fields_translated.append(field)
            fields_translated.extend(get_translation_fields(field))

        for field_name in set(self.fields):
            if field_name in fields_translated:
                self.fields.pop(field_name)

    class Meta:
        model = Extension
        exclude = ['user']

    def get_mirror_journals(self, obj):
        return obj.mirror_journals.all().values_list('issns', flat=True)

    def get_licensed_journals(self, obj):
        return obj.licensed_journals.all().values_list('issns', flat=True)

    def get_institution_logo(self, obj):
        if obj.institution_logo:
            return obj.institution_logo.url
        return None

    def get_translations(self, obj):
        # The resulting API output has the form of
        # {..., "translations": {"de": {...}, "en": {...}}}

        translations = {}
        for lang, _ in settings.LANGUAGES:
            serialized = TranslationSerializer(instance=obj, lang=lang).data

            translations[lang] = {}
            for key, value in serialized.items():
                key_stripped = key.removesuffix('_' + lang)
                translations[lang][key_stripped] = value
        return translations
