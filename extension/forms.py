from django import forms
import nh3

from .models import Extension


class ExtensionForm(forms.ModelForm):
    additional_text_en = forms.CharField(widget=forms.Textarea)
    additional_text_de = forms.CharField(widget=forms.Textarea)

    class Meta:
        model = Extension
        fields = '__all__'

    def strip_non_text_formatting(self, text):
        # allow HTML for text formatting and strip the rest
        return nh3.clean(text, tags={'b', 'strong', 'i', 'em', 'mark', 'small',
                                     'del', 'ins', 'sub', 'sup', 'p', 'br'})

    def clean_additional_text_en(self):
        return self.strip_non_text_formatting(
            self.cleaned_data['additional_text_en']
        )

    def clean_additional_text_de(self):
        return self.strip_non_text_formatting(
            self.cleaned_data['additional_text_de']
        )
