from django.conf import settings
from django.contrib.auth.models import User
from django.db import models

from recommender.models import License


class Extension(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    name = models.CharField(max_length=150, null=True, blank=True)

    additional_text = models.CharField(
        max_length=4000,
        help_text="Dieser Text erscheint in einem grauen Kästchen über der Eingabemaske. HTML tags zur Textformatierung werden unterstützt (z.B '<b>')",
        default="Zusätzliche Informationen für die Möglichkeiten einer Übernahme von Publikationskosten durch #Einrichtung#: Zu jeder von B!SON ermittelten Zeitschrift wird mit einem farbigen Badge angegeben, ob für Angehörige des/der #Einrichtung# eine Erstattung der Publikationskosten grundsätzlich möglich ist (gelb), die Zeitschrift von einem Open-Access-Vertrag abgedeckt ist, an dem #Einrichtung# teilnimmt (blau), die Zeitschrift keine APCs erhebt (grün) oder die Kosten für eine Publikation in der Zeitschrift voraussichtlich von #Institution# nicht übernommen werden (rot). Bitte informieren Sie sich über die unten angegebenen Kontaktmöglichkeiten über weitere Fördervoraussetzungen.",
    )
    support_url = models.URLField(
        max_length=2000,
        null=True,
        blank=True,
        help_text="Link zu Website mit Förderinformationen. Erscheint unter der Suchmaske bzw. den Suchergebnissen in einem grauen Kästchen.",
    )
    support_linktext = models.CharField(
        max_length=5000,
        default="Weitere Informationen zur Finanzierung von Publikationskosten an #Einrichtung#",
        null=True,
        blank=True,
    )
    institution_logo = models.ImageField(
        upload_to='logos/',
        null=True,
        blank=True,
        help_text='Logo der Institution welches bei den Informationen unten auf der Seite erscheint.')
    contact_mail = models.EmailField(
        max_length=500,
        null=True,
        verbose_name="E-Mail for contact",
        help_text="E-mail einer Kontaktperson oder Funktionsadresse. Erscheint unter der Suchmaske bzw. den Suchergebnissen in einem grauen Kästchen.",
    )
    contact_linktext = models.CharField(
        max_length=4000, default="Ansprechpartner für weitere Informationen"
    )

    badge_text_green = models.CharField(
        max_length=50,
        default="APC-frei",
        help_text="Der grüne Badge markiert APC-freie Zeitschriften.",
    )
    badge_tooltip_green = models.CharField(
        max_length=500,
        default="Diese Zeitschrift erhebt keine Publikationsgebühren (APCs). Bitte versichern Sie sich dazu auf der Website der Zeitschrift. / This journal does not charge article processing charges. Please verify on the journal's website.",
    )

    badge_text_yellow = models.CharField(
        max_length=50,
        default="Ggf. Kostenübernahme",
        help_text="Der gelbe Badge markiert Zeitschriften, die laut den von Ihnen eingegebenen Kriterien - Preisobergrenze und Lizenzen - an Ihrer Einrichtung förderbar sein können.",
    )
    badge_tooltip_yellow = models.CharField(
        max_length=500,
        default="Diese Zeitschrift erfüllt voraussichtlich die Bedingungen für eine Kostenübernahme durch den Publikationsfonds von #Institution#. Bitte wenden Sie sich an den unten angegebenen Kontakt. / This journal's APC should be refundable by the publication funds of #institution#. Please contact us via the e-mail address or website below.",
    )

    badge_text_red = models.CharField(
        max_length=50,
        default="Nicht förderfähig",
        help_text=" Der rote Badge markiert Zeitschriften, die die angegebenen Förderkriterien - Preisobergrenze, Lizenzen - nicht erfüllen.",
    )
    badge_tooltip_red = models.CharField(
        max_length=500,
        default="Diese Zeitschrift erfüllt die Förderbedingungen von/der #Institution# voraussichtlich nicht und es ist keine Kostenübernahme durch den Publikationsfonds möglich. Für weitere Fragen nehmen Sie bitte Kontakt auf. / This journal is presumably not applicable for refunding by the publication funds of ’institution#. Please contact the #institutions#'s library in case of further questions.",
    )

    badge_text_red = models.CharField(
        max_length=50,
        default="Nicht förderfähig",
        help_text=" Der rote Badge markiert Zeitschriften, die die angegebenen Förderkriterien - Preisobergrenze, Lizenzen - nicht erfüllen.",
    )
    badge_tooltip_red = models.CharField(
        max_length=500,
        default="Diese Zeitschrift erfüllt die Förderbedingungen von/der #Institution# voraussichtlich nicht und es ist keine Kostenübernahme durch den Publikationsfonds möglich. Für weitere Fragen nehmen Sie bitte Kontakt auf. / This journal is presumably not applicable for refunding by the publication funds of ’institution#. Please contact the #institutions#'s library in case of further questions.",
    )

    badge_text_blue = models.CharField(
        max_length=50,
        default="von Vertrag abgedeckt",
        help_text=" Der blaue Badge markiert Zeitschriften, die von Verträgen (DEAL, institutionelle Verträge) abgedeckt sind und für die keine Gebühren anfallen. Hinterlegung der Zeitschriften unter Licensed Journals",
    )
    badge_tooltip_blue = models.CharField(
        max_length=500,
        default="Für diese Zeitschrift fallen für Angehörige der #Einrichtung# keine Gebühren an, da sie von einem Vertrag abgedeckt wird. Bitte wenden Sie sich bei Rückfragen an den unten angegebenen Kontakt. / For this journal, members of #institution# can publish without payment of APCs, since it is covered by a contract. Please contact us via the below e-mail address in case of any questions.",
    )

    licenses = models.ManyToManyField(
        License,
        blank=True,
        help_text="Wählen Sie aus den Lizenzangaben der Zeitschriften diejenigen aus, die Voraussetzung für eine Förderung sind.",
    )
    price_limit = models.IntegerField(
        verbose_name="Upper price limit (in €)", null=True, blank=True
    )

    class MirrorJournalHandling(models.TextChoices):
        IGNORE = "IGNORE", "Ignore"
        HIDE = "HIDE", "Hide"
        LABEL = "LABEL", "Label"

    mirror_handling = models.CharField(
        max_length=10,
        choices=MirrorJournalHandling.choices,
        default=MirrorJournalHandling.IGNORE,
    )

    def __str__(self):
        return f'Extension ({self.name or self.id})'


class MirrorJournal(models.Model):
    issns = models.CharField(max_length=20)
    name = models.CharField(max_length=200, null=True, blank=True)
    publisher = models.CharField(max_length=200, null=True, blank=True)
    extension = models.ForeignKey(
        Extension, on_delete=models.CASCADE, related_name="mirror_journals"
    )


class LicensedJournal(models.Model):
    issns = models.CharField(max_length=20)
    name = models.CharField(max_length=200, null=True, blank=True)
    publisher = models.CharField(max_length=200, null=True, blank=True)
    extension = models.ForeignKey(
        Extension, on_delete=models.CASCADE, related_name="licensed_journals"
    )

    class Meta:
        verbose_name = 'Journal with OA agreement'
        verbose_name_plural = 'Journals with OA agreement'

    def __str__(self):
        return f'Journal ({self.issns})'
