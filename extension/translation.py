from modeltranslation.translator import register, TranslationOptions

from .models import Extension


@register(Extension)
class ExtensionTranslationOptions(TranslationOptions):
    fields = ('additional_text', 'support_linktext', 'contact_linktext',
              'badge_text_green', 'badge_tooltip_green', 'badge_text_yellow',
              'badge_tooltip_yellow', 'badge_text_red', 'badge_tooltip_red',
              'badge_text_blue', 'badge_tooltip_blue', 'support_url')
