from django import forms
from django.contrib import admin, auth
from import_export import resources
from import_export.admin import ImportExportModelAdmin
from modeltranslation.admin import TranslationAdmin

from .models import Extension, MirrorJournal, LicensedJournal
from .forms import ExtensionForm


class FilteredModel(admin.ModelAdmin):
    """Filtered model so the user only see/modify their objects"""

    def save_model(self, request, obj, form, change):
        if not request.user.is_superuser:
            if hasattr(obj, 'user'):
                obj.user = request.user
            elif obj.extension not in request.user.extension_set.all():
                raise ValueError('Setting extension not allowed')
        obj.save()

    def get_fields(self, request, obj):
        fields = super().get_fields(request, obj)
        if request.user.is_superuser:
            return fields
        return fields

    def get_queryset(self, request):
        qs = super().get_queryset(request)
        if request.user.is_superuser:
            return qs
        if 'user' in [f.name for f in qs.model._meta.fields]:
            return qs.filter(user=request.user)
        else:
            return qs.filter(extension__user=request.user)

    def has_change_permission(self, request, obj=None):
        if not obj:
            # the changelist itself
            return True
        return ((hasattr(obj, 'extension') and obj.extension in request.user.extension_set.all()) or
                (hasattr(obj, 'user') and obj.user == request.user) or
                request.user.is_superuser)


@admin.register(Extension)
class ExtensionFiltered(FilteredModel, TranslationAdmin):
    list_display = ["pk", "user", "name", "contact_mail"]
    form = ExtensionForm
    save_as = True

    def _get_form_or_formset(self, request, obj, **kwargs):
        # get_fields does not work with TranslationAdmin
        # https://github.com/deschler/django-modeltranslation/issues/473
        kwargs = super()._get_form_or_formset(request, obj, **kwargs)
        if request.user.is_superuser:
            return kwargs

        exclude = list(kwargs['exclude'])
        exclude.append('user')
        kwargs.update({'exclude': exclude})

        return kwargs


# used to enable importing data from file in django admin
class MirrorJournalResource(resources.ModelResource):
    def before_import_row(self, row, **kwargs):
        if not kwargs['user'].is_superuser or 'user' not in row:
           row['user'] = kwargs['user'].id
        return super().before_import_row(row, **kwargs)

    def get_user_visible_fields(self):
        # don't show the "user" as a required field when importing via file
        fields = super().get_user_visible_fields()
#       return fields
        return [field for field in fields if field.column_name != 'user']

    class Meta:
        model = MirrorJournal
        exclude = ('id')
        import_id_fields = ('issns', 'extension')


class MirrorJournalAdminForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        # only show user's extensions in foreignkey select dropdown
        if not self.__user.is_superuser:
            self.fields['extension'].queryset = self.fields['extension'].queryset.filter(user=self.__user)

    @classmethod
    def set_user(cls, user):
        cls.__user = user

    class Meta:
        model = MirrorJournal
        fields = "__all__"


@admin.register(MirrorJournal)
class MirrorJournalResourceAdmin(FilteredModel, ImportExportModelAdmin):
    resource_classes = [MirrorJournalResource]
    list_display = ['issns', 'extension', 'name', 'publisher']
    form = MirrorJournalAdminForm

    def get_import_resource_kwargs(self, request, *args, **kwargs):
        kwargs = super().get_resource_kwargs(request, *args, **kwargs)
        kwargs.update({"user": request.user})
        return kwargs

    def get_list_filter(self, request):
        # add 'user' to list_filter if request is from admin
        if request.user.is_superuser:
            return ['publisher', 'extension']
        else:
            return ['publisher']

    def get_form(self, request, obj=None, **kwargs):
        form_class = super().get_form(request, obj, **kwargs)
        form_class.set_user(request.user)

        return form_class


class LicensedJournalAdminForm(forms.ModelForm):
    class Meta:
        model = LicensedJournal
        fields = "__all__"

class LicensedJournalResource(MirrorJournalResource):
    form = MirrorJournalAdminForm

    class Meta:
        model = LicensedJournal


@admin.register(LicensedJournal)
class LicensedJournalResourceAdmin(MirrorJournalResourceAdmin):
    resource_classes = [LicensedJournalResource]
